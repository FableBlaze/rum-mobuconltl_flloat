package rum.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.Vector;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XEventImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.processmining.plugins.mobuconltl.MoBuConLTL;
import org.xml.sax.InputSource;

public class Server {
	private static ServerSocket server;
	private static int port = 9875;
	static boolean LTL2Automaton = false;

	private static MoBuConLTL monitor = null;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		server = new ServerSocket(port);
		monitor = new MoBuConLTL();
		String letto = null;
		String model = null;

		System.out.println("\nWaiting for client request");
		Socket socket = server.accept();
		model = receive(socket);
		System.out.println("model: " + model);

		try {
			@SuppressWarnings("rawtypes")
			Vector weights = new Vector();
			socket = server.accept();
			letto = receive(socket);
			while(letto != null && !letto.equals("END_WEIGHTS")){
				System.out.println(letto);
				weights.add(letto);
				socket = server.accept();
				letto = receive(socket);
			}
			int timeWindowSize = 1;

			monitor.accept(removeProbabilities(model),weights,timeWindowSize);

			while(!letto.contains("</Entry>")){
				System.out.println(letto);
				socket = server.accept();
				letto = receive(socket);
			}


			while (!letto.equalsIgnoreCase("exit")) {	
				System.out.println("Received Message: " + letto);

				SAXBuilder parserSax = new SAXBuilder();
				Document mess = parserSax.build(new InputSource(new StringReader(letto)));
				Element rootMess = mess.getRootElement();

				String eventName = rootMess.getChild("WorkflowModelElement").getText();
				String piID = rootMess.getChild("ProcessInstanceID").getText();
				long timestamp = new Long(rootMess.getChild("Timestamp").getText()).longValue();

				XEvent completeEvent = new XEventImpl();
				XTimeExtension.instance().assignTimestamp(completeEvent, timestamp);
				XConceptExtension.instance().assignName(completeEvent, eventName/*.toLowerCase()*/);

				XTraceImpl sentEvents = new XTraceImpl(new XAttributeMapImpl());
				XConceptExtension.instance().assignName(sentEvents, piID);
				sentEvents.add(completeEvent);

				XLog emptyLog = XFactoryRegistry.instance().currentDefault().createLog();
				emptyLog.add(sentEvents);

				Map<String, Object> analysis;
				if(eventName.equals("complete")){
					analysis = monitor.simple(emptyLog, piID, true);
				} else {
					analysis = monitor.simple(emptyLog, piID, false);
				}

				System.out.println("Sending analysis: " + analysis);
				send(socket, analysis);

				socket = server.accept();
				letto = receive(socket);
			}

			server.close();
			System.out.println("Shutting down now");
			System.exit(0);

		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}

	private static String removeProbabilities(String model) {
		String constraints = "";
		String[] splitModel = model.split("#");
		for (String string : splitModel[1].split(",")) {
			constraints = constraints + "," + string.split(";")[0];
		}
		splitModel[1] = constraints.substring(1);
		model = String.join("#", splitModel);
		return model;
	}

	//Get Trace and/or model from client
	private static String receive(Socket socket) throws IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		return (String) ois.readObject();
	}

	private static void send(Socket socket, Map<String, Object> msg) throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
		oos.writeObject(msg);
	}
}
