package org.processmining.plugins.mobuconltl.invoker;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.out.XesXmlSerializer;
import org.jdom.JDOMException;
import org.processmining.plugins.declareminer.visualizing.AssignmentViewBroker;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import com.fluxicon.slickerbox.colors.SlickerColors;

public class LogReplayer {



	/**
	 * 
	 */
	//public static final String DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss.SSS+kk:zz";
	//public static final String DATE_FORMAT = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'+'K";
	private int value;
	public static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss:S";
	private static final long serialVersionUID = 1561407447457027863L;
	private static JTextField  windSize;
	private JPanel contentPane;
	private static AssignmentViewBroker broker = null;
	private String brokerFilePath;
	public static final String CASE_COMPLETE = "case_complete";
	private static final int MONITOR_TAB = 0;
	private static final int HEALTH_TAB = 1;
	private static final int DIAGNOSTICS_TAB = 2;
	private Color backgroundColor = SlickerColors.COLOR_BG_4;
	private Color chartBackgroundColor = new Color(232,232,232);
	private JRadioButton twentyfour;
	private JRadioButton sixty;
	private JRadioButton twelve;
	private JRadioButton six;
	private JRadioButton three;
	private JButton search;
	private String shipType = "REF MODEL: no model selected";
	//private static final String path = "/it/unibo/ai/rec/client/icons/";
	private int selectedTab;
	private boolean showV;
	private boolean showA;
	private boolean searching;
	//	private String confPath;
	private static JCheckBox check;
	private Map<String, Object> analysis;
	boolean primo = true;

	private static HashMap<String, Double> weights;
	private Action startAction;
	//	private ImageLozengeButton eventButton;
	//private XLog log;
	//	private Vector infos;
	protected static XLog log = null;

	//	private boolean busy = false;

	@SuppressWarnings("serial")
	private void initActions() {
		startAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setEnabled(false);
	
			}

		};
		startAction.setEnabled(true);

	}



	public LogReplayer(String confPath) throws JDOMException, IOException {
		initActions();
		weights = new HashMap<String, Double>();
		//outputContainer = createSlickerScrollPane();
	}

	public String getStringFromDoc(org.w3c.dom.Document doc)    {
		DOMImplementationLS domImplementation = (DOMImplementationLS) doc.getImplementation();
		LSSerializer lsSerializer = domImplementation.createLSSerializer();
		return lsSerializer.writeToString(doc);   
	}

	public static void startReplay() {
		XesXmlSerializer serializer = new XesXmlSerializer();
		if(log==null){
			System.out.println("LOG MISSING!");
			return;
		}
		String host = "localhost";
		try{
			long times=8000;
			HashMap<Long,Vector<String>> events =  new HashMap<Long, Vector<String>>();
			
			
			Vector<Long> timestamps = new Vector<Long>();

			for(XTrace trace : log){
				long old = -1;
				for(XEvent event: trace){
					XAttributeMap eventAttributeMap = event.getAttributes();
					long current = XTimeExtension.instance().extractTimestamp(event).getTime();
					if(current<=old){
						old = old +1;
					}else{
						old = current;
					}


					XFactory factory = XFactoryRegistry.instance().currentDefault();
					XLog logTosend = factory.createLog();
					logTosend.setAttributes(log.getAttributes());
					XTrace traceToSend = factory.createTrace();
					traceToSend.setAttributes(trace.getAttributes());
					XEvent eventToSend = factory.createEvent();
					eventToSend.setAttributes(event.getAttributes());
					traceToSend.add(eventToSend);
					logTosend.add(traceToSend);
					
					Socket socket = new Socket (host,8080);
					socket.isConnected(); 
					OutputStream writeOnTheSocket = socket.getOutputStream();
					serializer.serialize(logTosend, writeOnTheSocket);
					writeOnTheSocket.flush();
					writeOnTheSocket.close();
					socket.close();
					Thread.sleep(times);
				}
			}
			return;
		}catch (Exception ex){ex.printStackTrace();} 

	};

	public static void main(String[] args) {
		String inputLogFileName = "C://Users/user/Desktop/log.xes";
		XesXmlParser parser = new XesXmlParser();
		if(parser.canParse(new File(inputLogFileName))){
			try {
				log = parser.parse(new File(inputLogFileName)).get(0);
				startReplay();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
//		try {
//			if(args.length>0){
//				new LogReplayer(args[0]).setVisible(true);
//			}else{
//				new LogReplayer("conf.xml").setVisible(true);
//			}
//		} catch (JDOMException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}




}



