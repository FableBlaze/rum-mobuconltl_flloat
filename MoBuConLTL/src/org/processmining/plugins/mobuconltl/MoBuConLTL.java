package org.processmining.plugins.mobuconltl;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XExtendedEvent;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XEventImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.processmining.plugins.declareminer.enumtypes.DeclareTemplate;
import org.processmining.plugins.declareminer.visualizing.ConstraintDefinition;

import RuntimeVerification.ExecutableAutomaton;
import RuntimeVerification.RVTempFalse;
import RuntimeVerification.RVTempTrue;
import RuntimeVerification.RVTrue;
import main.LDLfAutomatonResultWrapper;
import main.LTLfAutomatonResultWrapper;
import main.Main;
import net.sf.tweety.logics.pl.syntax.Proposition;
import net.sf.tweety.logics.pl.syntax.PropositionalSignature;
import rationals.Automaton;
import rationals.State;

/**
 * 
 * @author Fabrizio Maria Maggi
 * 
 */

public class MoBuConLTL {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3042748916288208677L;
	private static DateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:S");

	//private File integrate = new File("C:/Users/Fabrizio/Desktop/integrate.rdf");
	//PrintWriter pw = new PrintWriter(new FileWriter(integrate));
	//private WeightedDeclareModel wmodel;
	//private String[] constraints;
	////private String[] forms;
	//private String formula;
	//private final Hashtable formulaTable;

	//private final Hashtable hash = new Hashtable();

	//private Xes2RecTraceTranslator traceTranslator;
	//private RecEngine recProxy;

	/*
	 * IMPLEMENTARE METODI ACCEPT SIMPLE E GET NAME
	 * 
	 * public <R, L> R simple(final Session session, final XLog emptylog, final
	 * String langauge, final L query, final boolean false true se hai fatto)
	 */




	protected String convert(XTrace trace, int pos) {
		XExtendedEvent ev = XExtendedEvent.wrap(trace.get(pos));
		return "" + ev.getTimestamp().getTime();
	}

	public String getName() {
		return "Mobucon LTL";
	}
	
	private FormulasSet fs;
	private Hashtable replays;
	private Map<String, StateObject> currentStateMap = new HashMap<>();
	private Vector weights;
	private int timeWindow;
	private Map<String, XTrace> traceMap = new HashMap<>();
	private Set<String> alphabet;
	//private Automaton automaton;
	private Automaton[] pAut;

	
	
	public boolean accept(Object model, Vector weights, int timeWindow) {
		this.weights = weights;
		this.timeWindow = timeWindow;
		replays = new Hashtable();
		try {
			fs = new FormulasSet();
			String ldlModel = (String)model;
			fs.setLdlmodel(ldlModel);
			String[] alphaStrings = ldlModel.split("#")[0].split(",");
			String[] forms = ldlModel.split("#")[1].split(",");
			//	int i = 0;
			alphabet = new HashSet<String>();
			alphabet.add("tau");
			for(String sym : alphaStrings){
				alphabet.add(sym);
			}
			//String formula = "(";

			//for (int i=0;i<forms.length;i++){
			//	if (i != (forms.length - 1)) {
			//		formula = formula + forms[i] + ")&&(";
			//	} else {
			//		formula = formula + forms[i] + ")";
			//	}
			//}
			//i++;
			//String[] labels = ldlModel.split("#")[3].split(",");
			fs.setForms(forms);
			fs.setFormula("true");
			String[] labels = ldlModel.split("#")[3].split(",");
			fs.setLabels(labels);
			pAut = new Automaton[forms.length];
			for (int j = 0; j < forms.length; j++) {
				String input = forms[j];
				PropositionalSignature autAlphabet = new PropositionalSignature();
				for(String symbol : alphabet){
					Proposition prop = new Proposition(symbol);
					autAlphabet.add(prop);
				}
//				Automaton automaton = null;
				if(ldlModel.split("#")[2].equals("ltl")){
					LTLfAutomatonResultWrapper result = Main.ltlfString2Aut(input, autAlphabet, true, true, false, false);
					pAut[j] = result.getAutomaton();
					System.out.println(input);
					System.out.println(pAut[j].toString());
				}else{
					LDLfAutomatonResultWrapper result = Main.ldlfString2Aut(input, autAlphabet, true, true, false, false);
					pAut[j] = result.getAutomaton();
					System.out.println(input);
					System.out.println(pAut[j].toString());
				}
				
//				ExecutableAutomaton ea = new ExecutableAutomaton(automaton);
//				pAut[j] = automaton; //conjunction.getAutomaton().op.reduce();
			}
			PropositionalSignature autAlphabet = new PropositionalSignature();
			for(String symbol : alphabet){
				Proposition prop = new Proposition(symbol);
				autAlphabet.add(prop);
			}
		//	automaton = null;
		//	if(ldlModel.split("#")[2].equals("ltl")){
		//		LTLfAutomatonResultWrapper result = Main.ltlfString2Aut("true", autAlphabet, true, false, true, false);
		//		automaton = result.getAutomaton();
		//	}else{
		//		LDLfAutomatonResultWrapper result = Main.ldlfString2Aut("true", autAlphabet, true, false, true, false);
		//		automaton = result.getAutomaton();
		//	}
			
//			ExecutableAutomaton ea = new ExecutableAutomaton(automaton);
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream("ltlfAutomaton.gv");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			//	modelFile.delete();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}

	public <R, L> R simple(final XLog availableItems, final L query,
			final boolean done) throws Exception {

		if ((query == null) || !(query instanceof String)) {
			return null;
		}
		boolean coloured = true;
		Hashtable output = null;
		XTrace lastTrace = availableItems.get(0);
		XTrace completeTrace;
		String traceId = XConceptExtension.instance().extractName(lastTrace);
		String[] forms = fs.getForms();
		StateObject currentState = null;
		ExecutableAutomaton[] pExecAut = null;
		//ExecutableAutomaton execAut = null;
		
		if(!currentStateMap.containsKey(traceId)) {
			String[][] matrix = new String[forms.length][1];
			pExecAut = new ExecutableAutomaton[forms.length];
		
			for (int j = 0; j < forms.length; j++) {
				ExecutableAutomaton ea = new ExecutableAutomaton(pAut[j]);
				pExecAut[j] = ea;
			}
			
			
			currentState = new StateObject();
			currentState.setpExecAut(pExecAut);
		//	currentState.setExecAut(new ExecutableAutomaton(automaton));
			currentState.setMatrix(matrix);
			currentState.setEventPos(-1);
			currentState.setHealth(1);
			currentState.setHealthWindow(new Vector());
			currentStateMap.put(traceId, currentState);
		} else {
			currentState = currentStateMap.get(traceId);
		}
		
		if (!traceMap.containsKey(traceId)) {
			completeTrace = XFactoryRegistry.instance().currentDefault().createTrace();
			traceMap.put(traceId, completeTrace);
		} else {
			completeTrace = traceMap.get(traceId);
		}
		XTrace trace = completeTrace;
		Iterator<XEvent> eventIterator = lastTrace.iterator();
		while (eventIterator.hasNext()) {
		    XEvent event = eventIterator.next();
		    completeTrace.add(event);	
		 }
		if (lastTrace.size() == 0) {
			lastTrace = completeTrace;
		}
		//Map<String, Object> a = new HashMap();
		String systemID = (String) query;
		XTrace replay = null;
		if (replays.containsKey(systemID)) {
			replay = (XTrace) replays.get(systemID);
		} else {
			replay = new XTraceImpl(new XAttributeMapImpl());
		}

		String[] constraints = fs.getLabels();
		String positive = "";
		String negative = "";
		String[][] matrix = null;
		double health = -1;
		Vector healthWindow = null;
		double eventAffection = 0;
		int startingIndex = traceMap.size() - 5;
		if (startingIndex < 0) {
			startingIndex = 0;
		}
		int evPos;
		healthWindow = currentState.getHealthWindow();
		if (healthWindow.size() >= timeWindow) {
			healthWindow.remove(0);
		}
		pExecAut = currentState.getpExecAut();
		//execAut = currentState.getExecAut();
//		pAut = currentState.getpAut();
//		aut = currentState.getAut();
		evPos = currentState.getEventPos() + 1;
		currentState.setEventPos(evPos);
		matrix = new String[constraints.length][evPos + 1];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < (matrix[0].length - 1); j++) {
				matrix[i][j] = currentState.getMatrix()[i][j];
			}
		}
		Iterator events = lastTrace.iterator();
		while (events.hasNext()) {
			XEvent ev = null;
			ev = (XEvent) events.next();
			XEvent completeEvent = new XEventImpl();
			XExtendedEvent eva = XExtendedEvent.wrap(ev);
			XTimeExtension.instance().assignTimestamp(completeEvent, eva.getTimestamp());
			XConceptExtension.instance().assignName(completeEvent, eva.getName());
			boolean violated = true;
			String label = "";
			//	String globdiag = "";
			//String result = "[";
			label = ((XAttributeLiteral) ev.getAttributes().get("concept:name")).getValue();
			if(!alphabet.contains(label)){
				label = "tau";
			}
			//System.out.println("EVENTOO"+ label);
		//	for (String r : execAut.declareNotFailingEvents()){
		//		System.out.println("PPPPPPPPPPPPPPP      "+ r);
		//	}

		//	System.out.println("YYYYYYYYYYYYYYYYYYYY      "+ label);
			//State current = execAut.getCurrentState();
			if (!done) {
				
				//if (execAut.declareNotFailingEvents().contains(label)) {
					violated = false;
					
				//}
				/*
				if (violated) {
					if(current!=null){
						for (String out : execAut.declareNotFailingEvents()) {
							//if ((out.isPositive())) {
							if (positive.equals("")) {
								//globdiag = out.getPositiveLabel();
								positive = out;
							} else {
								//globdiag = globdiag + " or " + out.getPositiveLabel();
								positive = positive + "," + out;
							}
							//}
						}
					}
					//	globdiag = "for this vessel type " + label + " was detected it was expected " + globdiag;
				} else {
					execAut.step(label);
					replay.add(completeEvent);
					replays.put(systemID, replay);
				} */
				//current = execAut.getCurrentState();
			//	if (!violated) {
					for (int i = 0; i < pAut.length; i++) {
						boolean violatedIth = true;
						State current = pExecAut[i].getCurrentState();
						if(current!=null){
							//for (String out : pExecAut[i].declareNotFailingEvents()) {
							if (pExecAut[i].declareNotFailingEvents().contains(label)) {
								violatedIth = false;
								//break;
							}
							//}
						}
						
						if (violatedIth) {
							eventAffection = eventAffection + new Double((String) weights.get(i)).doubleValue();
							/*
							 * if (!pinpointViol) { singlediag = ""; }
							 * singlediag = singlediag + constraints[i] + ", ";
							 * pinpointViol = true;
							 */
						} else {
							pExecAut[i].step(label);	
						}	
						current = pExecAut[i].getCurrentState();
						System.out.println("STATUS: "+ pExecAut[i].currentRVTruthValue().getClass());
						if (!violatedIth) {
							if (pExecAut[i].currentRVTruthValue().getClass().equals(RVTempTrue.class)) {
								matrix[i][evPos] = "poss.sat";

								//for (State state : current) {
								//	if (state.isAccepting()) {
								//		for (Transition t : state.getOutput()) {
								//			if (t.isAll() && t.getTarget().equals(state)) {

								//	matrix[i][evPos] = "sat";
								//		}
								//	}
								//}
								//	}

							} 

							if (pExecAut[i].currentRVTruthValue().getClass().equals(RVTrue.class)) {
								matrix[i][evPos] = "sat";

								//for (State state : current) {
								//	if (state.isAccepting()) {
								//		for (Transition t : state.getOutput()) {
								//			if (t.isAll() && t.getTarget().equals(state)) {

								//	matrix[i][evPos] = "sat";
								//		}
								//	}
								//}
								//	}

							}


							if (pExecAut[i].currentRVTruthValue().getClass().equals(RVTempFalse.class)) {
								matrix[i][evPos] = "poss.viol";
							}
						} else {
							matrix[i][evPos] = "viol";
							//pinpointViol = true;
						}

					}
				//} else {
				//	for (int i = 0; i < matrix.length; i++) {
				//		if(evPos>0){
				////			matrix[i][evPos] = matrix[i][evPos - 1];
					//	}else{
					//		matrix[i][evPos] = "poss.sat";
					//	}
						//						matrix[i][evPos] = "skipped";
				//	}
				//}
				/*new Vector();
				Vector conflictingSets = new Vector();
				if (coloured) {
					replay = (XTrace) replays.get(systemID);
					if (violated) {
						int g = 1;
						boolean conflViolated = false;
						while ((g <= forms.length) && conflictingSets.isEmpty()) {
							String[][] conflCandidates = DispositionsGenerator.generateDisp(forms, g);
							int i = 0;
							while (i < conflCandidates.length) {
								conflViolated = false;
								String formula = "(";
								for (int k = 0; k < conflCandidates[0].length; k++) {
									if (k != (conflCandidates[0].length - 1)) {
										formula = formula + conflCandidates[i][k] + ")&&(";
									} else {
										formula = formula + conflCandidates[i][k] + ")";
										;
									}
								}

								String input = formula;

//								LTLfFormulaParserLexer lexer = new LTLfFormulaParserLexer(new ANTLRInputStream(formula));
//								LTLfFormulaParserParser parser = new LTLfFormulaParserParser(new CommonTokenStream(lexer));
//								ParseTree tree = parser.expression();
//								LTLfVisitor visitor = new LTLfVisitor();
//								LTLfFormula formulaLTL = visitor.visit(tree);
//								LTLfFormula antinnfFormula = formulaLTL.antinnf();
//								LDLfFormula ldlff = antinnfFormula.toLDLf();
//								System.out.println(ldlff.nnf());
//								PropositionalSignature autAlphabet = formulaLTL.getSignature();
//								for(String symbol : alphabet){
//									Proposition prop = new Proposition(symbol);
//									autAlphabet.add(prop);
//								}
//								Automaton automaton = AutomatonUtils.ldlf2AutomatonDeclare(ldlff, autAlphabet);
//								automaton = new Reducer<>().transform(automaton);
//
//								execAut = new ExecutableAutomaton(automaton);
								PropositionalSignature autAlphabet = new PropositionalSignature();
								for(String symbol : alphabet){
									Proposition prop = new Proposition(symbol);
									autAlphabet.add(prop);
								}
								Automaton automaton = null;
								String ldlModel = fs.getLdlmodel();
								if(ldlModel.split("#")[2].equals("ltl")){
									LTLfAutomatonResultWrapper result = Main.ltlfString2Aut(formula, autAlphabet, true, true, false, true, false);
									automaton = result.getAutomaton();
								}else{
									LDLfAutomatonResultWrapper result = Main.ldlfString2Aut(formula, autAlphabet, true, true, false, true, false);
									automaton = result.getAutomaton();
								}
								execAut = new ExecutableAutomaton(automaton);
//								FileOutputStream fos = null;
								
								
								System.out.println(automaton);

								boolean used = false;
								if(replay==null){
									replay = new XTraceImpl(new XAttributeMapImpl());
								}
								Iterator moevents = replay.iterator();
								while (moevents.hasNext() || !used) {
									XEvent eve = null;
									if (!moevents.hasNext()) {
										used = true;
										eve = completeEvent;
									} else {
										eve = (XEvent) moevents.next();
									}
									label = "";
									//				globdiag = "";
									conflViolated = true;
									label = ((XAttributeLiteral) eve.getAttributes().get("concept:name")).getValue();
									if(!alphabet.contains(label)){
										label = "tau";
									}
									current = null;
									current = execAut.getCurrentState();
									if(current!= null){
										//for (Transition out : current.output()) {
										if (execAut.declareNotFailingEvents().contains(label)) {
											conflViolated = false;
											//		break;
										}
										//	}
									}
									//			System.out.println(str);
									if (conflViolated) {
										//for (Transition out : current.output()) {
										//	if ((out.isPositive())) {
										//			if (positive.equals("")) {
										//	globdiag = out.getPositiveLabel();
										//			positive = out.getPositiveLabel();
										//		} else {
										//	globdiag = globdiag + " or " + out.getPositiveLabel();
										//			positive = positive + "," + out.getPositiveLabel();
										//		}
										//		}
										//		if ((out.isNegative())) {
										//				boolean first = true;
										//				for (String nl : out.getNegativeLabels()) {
										//					if (negative.equals("")) {
										//	globdiag = "(!" + nl;
										//						negative = nl;
										//					} else {
										//						if (first) {
										//	globdiag = globdiag + " or (!" + nl;
										//						negative = nl;
										//						} else {
										//	globdiag = globdiag + " and !" + nl;
										//						negative = negative + "," + nl;
										//					}
										//				}
										//				first = false;
										//			}
										//	globdiag = globdiag + ")";
										//	}
										//	}
										//	globdiag = "for this vessel type " + label + " was detected it was expected " + globdiag;
									} else {
										execAut.step(label);
									}
								}
								if (conflViolated) {
									conflictingSets.add(conflCandidates[i]);
								}
								i++;
							}
							g++;
						}

						Vector finConfl = new Vector();
						for (int l = 0; l < conflictingSets.size(); l++) {
							finConfl.add(conflictingSets.get(l));
						}

						for (int i = 0; i < conflictingSets.size(); i++) {
							String[] currentConfl = (String[]) conflictingSets.get(i);
							boolean minimalConfl = true;
							int j = 0;
							if (currentConfl.length > 1) {
								while ((j < currentConfl.length) && minimalConfl) {
									String formula = "(";
									Vector formVect = new Vector();
									for (int l = 0; l < currentConfl.length; l++) {
										if (!currentConfl[l].equals(currentConfl[j])) {
											formVect.add(currentConfl[l]);
										}
									}
									for (int k = 0; k < formVect.size(); k++) {
										if (k != (formVect.size() - 1)) {
											formula = formula + formVect.get(k) + ")&&(";
										} else {
											formula = formula + formVect.get(k) + ")";
											;
										}
									}
									PropositionalSignature autAlphabet = new PropositionalSignature();
									for(String symbol : alphabet){
										Proposition prop = new Proposition(symbol);
										autAlphabet.add(prop);
									}
									Automaton automaton = null;
									String ldlModel = fs.getLdlmodel();
									if(ldlModel.split("#")[2].equals("ltl")){
										LTLfAutomatonResultWrapper result = Main.ltlfString2Aut(formula, autAlphabet, true, true, false, true, false);
										automaton = result.getAutomaton();
									}else{
										LDLfAutomatonResultWrapper result = Main.ldlfString2Aut(formula, autAlphabet, true, true, false, true, false);
										automaton = result.getAutomaton();
									}
									execAut = new ExecutableAutomaton(automaton);									execAut = new ExecutableAutomaton(automaton);


									Iterator moevents = replay.iterator();
									boolean used = false;
									while (moevents.hasNext() || !used) {
										XEvent eve = null;
										if (!moevents.hasNext()) {
											used = true;
											eve = completeEvent;
										} else {
											eve = (XEvent) moevents.next();
										}
										label = "";
										//	globdiag = "";
										conflViolated = true;
										label = ((XAttributeLiteral) eve.getAttributes().get("concept:name"))
												.getValue();
										if(!alphabet.contains(label)){
											label = "tau";
										}
										current = null;
										current = execAut.getCurrentState();
										if(current!=null){
											//for (Transition out : current.output()) {
											if (execAut.declareNotFailingEvents().contains(label)) {
												conflViolated = false;
												//	break;
											}
											//}
										}
										//	System.out.println(str);
										if (conflViolated) {
											if (!moevents.hasNext()) {
												minimalConfl = false;
											}
											//	for (Transition out : current.output()) {
											//		if ((out.isPositive())) {
											//			if (globdiag.equals("")) {
											//				globdiag = out.getPositiveLabel();
											//				positive = out.getPositiveLabel();
											//			} else {
											//				globdiag = globdiag + " or " + out.getPositiveLabel();
											//				positive = positive + "," + out.getPositiveLabel();
											//			}
											//		}
											//		if ((out.isNegative())) {
											//			boolean first = true;
											//			for (String nl : out.getNegativeLabels()) {
											//				if (globdiag.equals("")) {
											//					globdiag = "(!" + nl;
											//					negative = nl;
											//				} else {
											//					if (first) {
											//						globdiag = globdiag + " or (!" + nl;
											//						negative = nl;
											//					} else {
											//						globdiag = globdiag + " and !" + nl;
											//						negative = negative + "," + nl;
											//					}
											//				}
											//				first = false;
											//			}
											//			globdiag = globdiag + ")";
											//		}
											//	}
											//	globdiag = "for this vessel type " + label + " was detected it was expected " + globdiag;
										} else {
											execAut.step(label);
										}
									}
									j++;
								}
							}
							if (!minimalConfl) {
								finConfl.remove(currentConfl);
							}
						}

						if (!finConfl.isEmpty()) {
							for (int i = 0; i < finConfl.size(); i++) {
								String[] currentConfl = (String[]) finConfl.get(i);
								for (int j = 0; j < currentConfl.length; j++) {
									for (int l = 0; l < forms.length; l++) {
										if (currentConfl[j].equals(forms[l])) {
											if (currentConfl.length == 1) {
												matrix[l][evPos] = "viol";
												eventAffection = eventAffection
														+ new Double((String) weights.get(i)).doubleValue();
											} else {
												matrix[l][evPos] = "conflict";
											}
										}
									}
								}
							}
						}
					}
				}
*/
				//	if (true) {//(!pinpointViol && violated) || !violated || (pinpointViol && (!(label.equals("complete") && violated)))) {
				String result = "[";
				int intCounterMin;
				int intCounterMax;
				String INF = "inf";
				for (int i = 0; i < matrix.length; i++) {
					intCounterMin = 0;
					intCounterMax = 0;
					String oldStatus = matrix[i][0];
					for (int j = 0; j < matrix[0].length; j++) {
						if (matrix[i][j] == null) {
							matrix[i][j] = oldStatus;
						}
						if (oldStatus.equals("viol")) {
							matrix[i][j] = "viol";
						}
						if ((j == (matrix[0].length - 1)) && (i == (matrix.length - 1))) {
							if (!matrix[i][j].equals(oldStatus)) {
								result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
										+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax) + "]),";
								oldStatus = matrix[i][j];
								intCounterMin = intCounterMax;
							}
							result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j] + "),["
									+ convert(trace, intCounterMin) + "," + INF + "])]";
							intCounterMin = intCounterMax;
							intCounterMax++;
						} else {
							if (j == (matrix[0].length - 1)) {
								if (matrix[i][j].equals(oldStatus)) {
									result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
											+ "),[" + convert(trace, intCounterMin) + "," + INF + "]),";
								} else {
									result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
											+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
											+ "])," + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
													+ "),[" + convert(trace, intCounterMax) + "," + INF + "]),";
								}
							} else {
								if (!matrix[i][j].equals(oldStatus)) {
									result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
											+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
											+ "]),";
									oldStatus = matrix[i][j];
									intCounterMin = intCounterMax;
								}
							}
						}
						intCounterMax++;

					}
				}

				if (violated) {
					eventAffection = eventAffection + 1;
				}
				//Analysis h = new Analysis("Health of the system");
				healthWindow.add(eventAffection);
				double temp = 0;
				for (int h = 0; h < healthWindow.size(); h++) {
					temp = temp + ((Double) healthWindow.get(h)).doubleValue();
				}
				double fact = 0;
				for (int l = 0; l < weights.size(); l++) {
					fact = fact + new Double((String) weights.get(l)).doubleValue();
				}
				health = 1 - (temp / ((fact) * healthWindow.size()));
				XExtendedEvent extEv = new XExtendedEvent(ev);
				extEv.getTimestamp().getTime();
				String fluents = result;
				result = result.subSequence(1, result.length() - 1).toString(); //remove "[" and "]"
				Scanner scanner = new Scanner(result);
				scanner.useDelimiter("mholds_for");

				while (scanner.hasNext()) {
					String mviStr = scanner.next();
					mviStr = mviStr.subSequence(1, mviStr.length() - 1).toString(); //remove "(" and ")"
					int removeTail = scanner.hasNext() ? 2 : 1;
					mviStr = mviStr.substring(0, mviStr.length() - removeTail); //remove "]," or "]"
					StringTokenizer tokenizer = new StringTokenizer(mviStr, "[", true);

					String fluent = "";
					String token;
					while (tokenizer.hasMoreTokens()) {
						token = tokenizer.nextToken();
						if (tokenizer.hasMoreTokens()) {
							fluent += token;
						} else {
							fluent = fluent.substring(0, fluent.length() - 2); //remove ,[
							//	token.split(",")[0];
							//	token.split(",")[1];
						}
					}
					//a.put("isStopped", false);
					//if (premstopped) {
					//	a.put("prematureCompletion", premdiag);
					//}
					/*
					 * if (!premstopped && (pinpointViol && violated)) {
					 * a.put("single", singlediag); } if (!premstopped &&
					 * (pinpointViol && violated)) { a.put("global", globdiag);
					 * a.put("isStopped", true); }
					 */
					//if(violated){
					//	a.put("global", globdiag);
				}
				output = new Hashtable();
				if (violated) {
					output.put("global", "violation");
				}
				output.put("health", health);
				output.put("positive", positive);
				output.put("negative", negative);
				output.put("fluents", fluents);
				output.put("constraintNum", String.valueOf(constraints.length));
				output.put("eventName",XConceptExtension.instance().extractName(ev));
				output.put("traceID",XConceptExtension.instance().extractName(lastTrace));
				//a.put("fluent", fluent);
				//a.put("start", startTime);
				//	if (!endTime.equals("_")) {
				//		a.put("end", endTime);
				//	}

				/*
				 * } else {
				 * 
				 * //Analysis a = new Analysis("Business Constraints Status");
				 * if (label.equals("complete") && violated) {
				 * a.put("isStopped", true); } else { a.put("global",
				 * "GLOBAL VESSEL TYPE VIOLATION! " + globdiag); }
				 * a.put("health", health); a.put("positive", positive);
				 * a.put("negative", negative); //response.addAnalysis(a); }
				 */
				currentState.setpExecAut(pExecAut);
			//	currentState.setExecAut(execAut);
				currentState.setMatrix(matrix);
				currentState.setEventPos(evPos);
				currentState.setHealth(health);
				currentState.setHealthWindow(healthWindow);
			} else {
				//execAut.step(new EmptyTrace());
				for (int i = 0; i < matrix.length; i++) {
					if (matrix[i][evPos - 1].equals("poss.viol")) {
						matrix[i][evPos] = "viol";
					} else if (matrix[i][evPos - 1].equals("poss.sat")) {
						matrix[i][evPos] = "sat";
					} else {
						matrix[i][evPos] = matrix[i][evPos - 1];
					}
				}
				boolean premstopped = false;
				String premdiag = "";
				for (int i = 0; i < pAut.length; i++) {
					State current = pExecAut[i].getCurrentState();
					if (pExecAut[i].currentRVTruthValue().getClass().equals(RVTempTrue.class)||pExecAut[i].currentRVTruthValue().getClass().equals(RVTrue.class)) {
						//	matrix[i][evPos] = matrix[i][evPos - 1];
					} else {
						premstopped = true;
						//	matrix[i][evPos] = "viol";
						//eventAffection = eventAffection + new Double((String) weights.get(i)).doubleValue();
						if (premdiag.equals("")) {
							premdiag = constraints[i];
						} else {
							premdiag = premdiag + ", " + constraints[i];
						}
					}
				}
				String result = "[";
				if (premstopped) {
					premdiag = "Case completed with " + premdiag + " still pending!";
				}
				int intCounterMin;
				int intCounterMax;
				String INF = "inf";
				for (int i = 0; i < matrix.length; i++) {
					intCounterMin = 0;
					intCounterMax = 0;
					String oldStatus = matrix[i][0];
					for (int j = 0; j < matrix[0].length; j++) {
						if (matrix[i][j] == null) {
							matrix[i][j] = oldStatus;
						}
						if (oldStatus.equals("viol")) {
							matrix[i][j] = "viol";
						}
						if ((j == (matrix[0].length - 1)) && (i == (matrix.length - 1))) {
							if (!matrix[i][j].equals(oldStatus)) {
								result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
										+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax) + "]),";
								oldStatus = matrix[i][j];
								intCounterMin = intCounterMax;
							}
							result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j] + "),["
									+ convert(trace, intCounterMin) + "," + INF + "])]";
							intCounterMin = intCounterMax;
							intCounterMax++;
						} else {
							if (j == (matrix[0].length - 1)) {
								if (matrix[i][j].equals(oldStatus)) {
									result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
											+ "),[" + convert(trace, intCounterMin) + "," + INF + "]),";
								} else {
									result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
											+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
											+ "])," + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
													+ "),[" + convert(trace, intCounterMax) + "," + INF + "]),";
								}
							} else {
								if (!matrix[i][j].equals(oldStatus)) {
									result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
											+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
											+ "]),";
									oldStatus = matrix[i][j];
									intCounterMin = intCounterMax;
								}
							}
						}
						intCounterMax++;
					}
				}

				//Analysis h = new Analysis("Health of the system");
				healthWindow.add(eventAffection);
				double temp = 0;
				for (int h = 0; h < healthWindow.size(); h++) {
					temp = temp + ((Double) healthWindow.get(h)).doubleValue();
				}
				double fact = 0;
				for (int l = 0; l < weights.size(); l++) {
					fact = fact + new Double((String) weights.get(l)).doubleValue();
				}
				health = 1 - (temp / ((fact) * healthWindow.size()));
				String stringone = result;
				result = result.subSequence(1, result.length() - 1).toString(); //remove "[" and "]"
				Scanner scanner = new Scanner(result);
				scanner.useDelimiter("mholds_for");
				while (scanner.hasNext()) {
					String mviStr = scanner.next();
					mviStr = mviStr.subSequence(1, mviStr.length() - 1).toString(); //remove "(" and ")"
					int removeTail = scanner.hasNext() ? 2 : 1;
					mviStr = mviStr.substring(0, mviStr.length() - removeTail); //remove "]," or "]"
					StringTokenizer tokenizer = new StringTokenizer(mviStr, "[", true);

					String fluent = "";
					String token;
					while (tokenizer.hasMoreTokens()) {
						token = tokenizer.nextToken();
						if (tokenizer.hasMoreTokens()) {
							fluent += token;
						} else {
							fluent = fluent.substring(0, fluent.length() - 2); //remove ,[
							//token.split(",")[0];
							//token.split(",")[1];
						}
					}
					output = new Hashtable();
					if (premstopped) {
						output.put("prematureCompletion", premdiag);
					}
					output.put("health", health);
					output.put("positive", positive);
					output.put("negative", negative);
					output.put("fluents", stringone);
					output.put("constraintNum", String.valueOf(constraints.length));
					output.put("eventName",XConceptExtension.instance().extractName(ev));
					output.put("traceID",XConceptExtension.instance().extractName(lastTrace));
					//a.put("fluent", fluent);
					//a.put("start", startTime);
					//if (!endTime.equals("_")) {
					//	a.put("end", endTime);
					//}
				}
			}
		}
		return (R) output;
	}

	public static DeclareTemplate getTemplate(ConstraintDefinition constraint){
		if(constraint.getName().toLowerCase().equals("absence")){
			return DeclareTemplate.Absence;
		}
		if(constraint.getName().toLowerCase().equals("absence2")){
			return DeclareTemplate.Absence2;
		}
		if(constraint.getName().toLowerCase().equals("absence3")){
			return DeclareTemplate.Absence3;
		}
		if(constraint.getName().toLowerCase().equals("alternate precedence")){
			return DeclareTemplate.Alternate_Precedence;
		}
		if(constraint.getName().toLowerCase().equals("alternate response")){
			return DeclareTemplate.Alternate_Response;
		}
		if(constraint.getName().toLowerCase().equals("alternate succession")){
			return DeclareTemplate.Alternate_Succession;
		}
		if(constraint.getName().toLowerCase().equals("chain precedence")){
			return DeclareTemplate.Chain_Precedence;
		}
		if(constraint.getName().toLowerCase().equals("chain response")){
			return DeclareTemplate.Chain_Response;
		}
		if(constraint.getName().toLowerCase().equals("chain succession")){
			return DeclareTemplate.Chain_Succession;
		}
		if(constraint.getName().toLowerCase().equals("precedence")){
			return DeclareTemplate.Precedence;
		}
		if(constraint.getName().toLowerCase().equals("response")){
			return DeclareTemplate.Response;
		}
		if(constraint.getName().toLowerCase().equals("succession")){
			return DeclareTemplate.Succession;
		}
		if(constraint.getName().toLowerCase().equals("responded existence")){
			return DeclareTemplate.Responded_Existence;
		}
		if(constraint.getName().toLowerCase().equals("co-existence")){
			return DeclareTemplate.CoExistence;
		}
		if(constraint.getName().toLowerCase().equals("exclusive choice")){
			return DeclareTemplate.Exclusive_Choice;
		}
		if(constraint.getName().toLowerCase().equals("choice")){
			return DeclareTemplate.Choice;
		}
		if(constraint.getName().toLowerCase().equals("existence")){
			return DeclareTemplate.Existence;
		}
		if(constraint.getName().toLowerCase().equals("existence2")){
			return DeclareTemplate.Existence2;
		}
		if(constraint.getName().toLowerCase().equals("existence3")){
			return DeclareTemplate.Existence3;
		}
		if(constraint.getName().toLowerCase().equals("exactly1")){
			return DeclareTemplate.Exactly1;
		}
		if(constraint.getName().toLowerCase().equals("exactly2")){
			return DeclareTemplate.Exactly2;
		}	
		if(constraint.getName().toLowerCase().equals("init")){
			return DeclareTemplate.Init;
		}
		if(constraint.getName().toLowerCase().equals("not chain succession")){
			return DeclareTemplate.Not_Chain_Succession;
		}
		if(constraint.getName().toLowerCase().equals("not succession")){
			return DeclareTemplate.Not_Succession;
		}	 
		if(constraint.getName().toLowerCase().equals("not co-existence")){
			return DeclareTemplate.Not_CoExistence;
		}
		return null;
	}
}