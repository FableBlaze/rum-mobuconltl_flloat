package nl.tue.mobucon.client;

import it.unibo.ai.rec.common.TimeGranularity;
import it.unibo.ai.rec.engine.FluentsConverter;
import it.unibo.ai.rec.model.FluentsModel;
import it.unibo.ai.rec.model.NoGroupingStrategy;
import it.unibo.ai.rec.model.RecTrace;
import it.unibo.ai.rec.visualization.BasicDateEventOutputter;
import it.unibo.ai.rec.visualization.FluentChartContainer;
import it.unibo.ai.rec.visualization.FluentChartFactory;
import it.unibo.ai.rec.visualization.FluentChartStandardPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import org.deckfour.uitopia.ui.components.ImageLozengeButton;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XExtendedEvent;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XEventImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jfree.data.general.AbstractDataset;
import org.processmining.operationalsupport.client.InvocationException;
import org.processmining.operationalsupport.client.SessionHandle;
import org.processmining.operationalsupport.messages.reply.ResponseSet;
import org.processmining.plugins.declareminer.visualizing.AssignmentModel;
import org.processmining.plugins.declareminer.visualizing.AssignmentViewBroker;
import org.processmining.plugins.declareminer.visualizing.XMLBrokerFactory;
import org.processmining.plugins.mobuconltl.DeclareLanguage;
import org.processmining.plugins.mobuconltl.DeclareMonitorQuery;
import org.processmining.plugins.mobuconltl.MoBuConLTL;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.fluxicon.slickerbox.colors.SlickerColors;
import com.fluxicon.slickerbox.components.SlickerTabbedPane;
import com.fluxicon.slickerbox.factory.SlickerDecorator;
import com.fluxicon.slickerbox.factory.SlickerFactory;

public class MoBuConClient extends JFrame implements ActionListener, MouseListener, KeyListener{



	/**
	 * 
	 */
	//public static final String DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss.SSS+kk:zz";
	//public static final String DATE_FORMAT = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'+'K";
	private int value;
	public static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss:S";
	protected FluentsConverter converter;
	private FluentChartFactory factory;
	private static final long serialVersionUID = 1561407447457027863L;

	private static int PORT = 4444;
	private static String HOST;
	private Hashtable healthHash = new Hashtable();
	public static final String CASE_COMPLETE = "case_complete";
	private static final int MONITOR_TAB = 0;
	private static final int HEALTH_TAB = 1;
	private static final int DIAGNOSTICS_TAB = 2;
	private Color backgroundColor = SlickerColors.COLOR_BG_4;
	private Color chartBackgroundColor = new Color(232,232,232);
	private JRadioButton showViolated;
	private JRadioButton showAll;
	private JRadioButton searchRadioButton;
	private JButton search;
	private String shipType = "REF MODEL: no model selected";
	//private static final String path = "/it/unibo/ai/rec/client/icons/";
	private int selectedTab;
	private boolean showV;
	private boolean showA;
	private boolean searching;
//	private String confPath;
	private final Hashtable handles;
	private Map<String, Object> analysis;
	boolean primo = true;
	MoBuConLTL monitor = new MoBuConLTL();
	FluentChartContainer chartPanel;

	static {
		try {
			// Get hostname
			HOST = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
		}
	}
	private JTextField searchField;
	private Action startAction;
	//private Action endAction;
	//private Action eventAction;
	//	private JTextField eventField;
	private JScrollPane outputContainer;
	private JScrollPane instancesContainer;
	//private JPanel roundedPanelMon;
	private JPanel roundedPanelHealth;
	private JPanel roundedPanelDiag;
	private JPanel completeListPanel;
	private JPanel filterPanel;
	private JPanel searchPanel;
	//private JLabel premviolLabel;
	//private JLabel singleviolLabel;
	//private JLabel globalviolLabel;
	//private Vector violations;
	private JList diagnosticsList;
	//	private ImageLozengeButton eventButton;
	//private XLog log;
	//	private Vector infos;
	private SlickerTabbedPane tpInstances;
	private Hashtable violationsModels;
	private Hashtable healthGraphs;
	private Hashtable monitorGraphs;
	private Hashtable partialTraces;
	private Hashtable globalResponses;
	private Hashtable healths;
	private Hashtable types;
	private Vector instancesAll;
	private Vector instancesViol;
	private String selected = "";
	private Vector instances;
	private JList instancesList;
	private XTrace trace;
	//	private boolean busy = false;

	@SuppressWarnings("serial")
	private void initActions() {
		startAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//	initLog();
				setEnabled(false);
				//		endAction.setEnabled(true);
				//	eventAction.setEnabled(true);
				//	resetTrace();
				//initLog(/*String path*/);
				//processRequest("start");
				sendRequest();
			}

		};
		startAction.setEnabled(true);

		/*	endAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				setEnabled(false);
				startAction.setEnabled(true);
				eventAction.setEnabled(false);
				//partialTrace.add(XEventUtils.generateEvent(CASE_COMPLETE));
				try {Thread.sleep(1000);}catch(Exception ex){}
				//		processRequest("complete");
			}
		};
		endAction.setEnabled(false);

		eventAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				//initLog(eventField.getText());
				//initLog();
				//Iterator tracesIterator = log.iterator();
				//	while(tracesIterator.hasNext()){
				//XTrace trace = (XTrace)tracesIterator.next();
				//Iterator eventIterator = trace.iterator();
				//while(eventIterator.hasNext()){
				//	XExtendedEvent ate = new XExtendedEvent((XEvent)eventIterator.next());
				//	String eventName = ate.getName();
				//	Date timeStamp = ate.getTimestamp();
				//	partialTrace.add((XEvent)eventIterator.next());
				//	processRequest("gigi");
				//	processRequest("a");
				//if(eventField.getText().length()>0){
				//processRequest(eventField.getText());
				//}
				//}
				//	}

		 */
		/*sendRequest();*/
		/*JFileChooser fileChooser = new JFileChooser();
				int n = fileChooser.showOpenDialog(OSPoseidonDeclareMonitorClient.this);
				File f = null;
				if(n!= JFileChooser.CANCEL_OPTION && n!= JFileChooser.ERROR_OPTION){
					f = fileChooser.getSelectedFile();
					eventField.setText(f.getAbsolutePath());
					initLog(f.getAbsolutePath());
					setEnabled(false);
					startAction.setEnabled(true);

					//eventAction.setEnabled(true);
					resetTrace();
				}
		 */
		//}
	};
	//	eventAction.setEnabled(false);

	//	}


	/*private void initLog(String path){
		File file = new File(path);
		String filename = file.getName();
		InputStream input;
		try {
			input = new FileInputStream(file);

			long fileSizeInBytes = file.length();
			XParser parser;
			if (filename.toLowerCase().endsWith(".xes") || filename.toLowerCase().endsWith(".xes.gz")) {
				parser = new XesXmlParser();
			} else {
				parser = new XMxmlParser();
			}
			Collection<XLog> logs = parser.parse(file);

			//parse(new XContextMonitoredInputStream(input, fileSizeInBytes, new Progress()));
			Response globalResponse = null;
			// log sanity checks;
			// notify user if the log is awkward / does miss crucial information
			if (logs.size() == 0) {
				throw new Exception("No processes contained in log!");
			}

			log = logs.iterator().next();
			XConceptExtension.instance().assignName(log, filename);

			if (log.isEmpty()) {
				throw new Exception("No process instances contained in log!");
			}



		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} */

	private JScrollPane createSlickerScrollPane() {

		JScrollPane scrollpane = new JScrollPane();
		scrollpane.setOpaque(false);
		scrollpane.getViewport().setOpaque(false);
		scrollpane.setBorder(BorderFactory.createEmptyBorder());
		//scrollpane.setViewportBorder(BorderFactory.createLineBorder(new Color(
		//		10, 10, 10), 2));
		scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		SlickerDecorator.instance().decorate(scrollpane.getVerticalScrollBar(),
				new Color(0, 0, 0, 0), new Color(140, 140, 140),
				new Color(80, 80, 80));
		scrollpane.getVerticalScrollBar().setOpaque(false);

		SlickerDecorator.instance().decorate(scrollpane.getHorizontalScrollBar(),
				new Color(0, 0, 0, 0), new Color(140, 140, 140),
				new Color(80, 80, 80));
		scrollpane.getHorizontalScrollBar().setOpaque(false);
		return scrollpane;
	}


	private JScrollPane createSlickerScrollPane(JList list) {


		JScrollPane scrollpane = new JScrollPane(list);
		scrollpane.setOpaque(false);
		scrollpane.getViewport().setOpaque(false);
		scrollpane.setBorder(BorderFactory.createEmptyBorder());
		//scrollpane.setViewportBorder(BorderFactory.createLineBorder(new Color(
		//		10, 10, 10), 2));
		scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		SlickerDecorator.instance().decorate(scrollpane.getVerticalScrollBar(),
				new Color(0, 0, 0, 0), new Color(140, 140, 140),
				new Color(80, 80, 80));
		scrollpane.getVerticalScrollBar().setOpaque(false);

		SlickerDecorator.instance().decorate(scrollpane.getHorizontalScrollBar(),
				new Color(0, 0, 0, 0), new Color(140, 140, 140),
				new Color(80, 80, 80));
		scrollpane.getHorizontalScrollBar().setOpaque(false);
		return scrollpane;
	}



	public MoBuConClient(String confPath) throws JDOMException, IOException {
		super("MoBuCon Client");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		try {
			factory = new FluentChartFactory(TimeGranularity.MILLIS, new BasicDateEventOutputter(
					TimeGranularity.MILLIS, DATE_FORMAT), false,"stateColors.properties","fluentColors.properties");
		} catch (Exception e) {
			e.printStackTrace();
		}
		//this.confPath = confPath;
	//	File configuration = new File(confPath); 
	//	SAXBuilder parserSax = new SAXBuilder();
	//	org.jdom.Document doc = parserSax.build(configuration);
	//	Element root = doc.getRootElement();
		HOST = "localhost";//root.getChild("host").getText();
		PORT = 4444;//new Integer(root.getChild("port").getText()).intValue();
		instancesAll = new Vector();
		instancesViol  = new Vector();
		violationsModels = new Hashtable();
		healthGraphs = new Hashtable();
		handles = new Hashtable();
		monitorGraphs = new Hashtable();
		partialTraces = new Hashtable();
		globalResponses = new Hashtable();
		types= new Hashtable();
		healths = new Hashtable();
		healthHash = new Hashtable();
		//infos = new Vector();
		//partialTrace = new XTraceImpl(new XAttributeMapImpl());

		initActions();

		//instances = new Vector();
		instancesList = new JList();
		instancesList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int[] selectedIndices = instancesList.getSelectedIndices();
				if(selectedIndices.length >0){
					if(!selected.equals(((InstanceInfo)instances.get(selectedIndices[0])).getId()))
						instancesSelectionChanged();
				}
			}
		});
		instancesList.setCellRenderer(new ListRenderer());
		//instancesList.setModel(new ListModel(instances));
		outputContainer = createSlickerScrollPane();
		instancesList.setBackground(Color.black);
		instancesList.setForeground(Color.yellow);
		instancesList.addMouseListener(this);
		instancesContainer = createSlickerScrollPane(instancesList);
		instancesContainer.setSize(200, 800);
		instancesContainer.setPreferredSize(new Dimension(200,800));
		instancesContainer.setMaximumSize(new Dimension(200,800));
		instancesContainer.setMinimumSize(new Dimension(200,800));
		instancesContainer.addMouseListener(this);
		SlickerFactory factory = SlickerFactory.instance();
		search = factory.createButton("search");
		search.addActionListener(this);
		search.setActionCommand("searching");
		searchField = new JTextField();
		searchField.addMouseListener(this);
		searchField.addKeyListener(this);
		searchField.setSize(new Dimension(100, 20));
		searchField.setMaximumSize(new Dimension(100, 20));
		searchField.setMinimumSize(new Dimension(100, 20));
		searchField.setPreferredSize(new Dimension(100, 20));
		searchPanel = factory.createRoundedPanel();
		searchPanel.setLayout(new BorderLayout());
		searchPanel.add(searchField, BorderLayout.WEST);
		//searchPanel.add(search, BorderLayout.EAST);
		search.setEnabled(false);
		searching = false;
		showA = true;
		showV = false;
		showAll= factory.createRadioButton("show All");
		showAll.addActionListener(this);
		showAll.setActionCommand("showAll");
		showAll.setSelected(true);
		showViolated = factory.createRadioButton("show warnings");
		showViolated.addActionListener(this);
		showViolated.setActionCommand("showViolated");
		searchRadioButton = factory.createRadioButton("search ID");
		searchRadioButton.addActionListener(this);
		searchRadioButton.setActionCommand("search");
		ButtonGroup bg = new ButtonGroup();
		bg.add(showAll);
		bg.add(showViolated);
		bg.add(searchRadioButton);
		JPanel contentPane = factory.createRoundedPanel();
		JPanel rbPanel = factory.createRoundedPanel();
		rbPanel.setLayout(new BorderLayout());
		rbPanel.add(showAll, BorderLayout.NORTH);
		rbPanel.add(showViolated,BorderLayout.CENTER);
		rbPanel.add(searchRadioButton, BorderLayout.SOUTH);
		filterPanel= factory.createRoundedPanel();
		filterPanel.setLayout(new BorderLayout());
		filterPanel.add(rbPanel, BorderLayout.NORTH);
		filterPanel.add(searchPanel, BorderLayout.SOUTH);
		//filterPanel.add(comp)
		completeListPanel = factory.createRoundedPanel();
		completeListPanel.setLayout(new BorderLayout());
		completeListPanel.add(instancesContainer, BorderLayout.CENTER);
		completeListPanel.add(filterPanel, BorderLayout.PAGE_END);
		//TypeToggleButton b = new TypeToggleButton("a","b",Color.red);
		//b.setEnabled(false);
		contentPane.setBackground(backgroundColor);
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout());

		JToolBar toolBar = new JToolBar();
		toolBar.setOpaque(false);
		toolBar.setFloatable(false);

		ImageLozengeButton startButton = new ImageLozengeButton(ImageLoader
				.load("next_white_30x30.png"), "Start", new Color(0, 90, 0),
				new Color(0, 140, 0), 4);
		startButton.setLabelColor(Color.white);
		startButton.setAction(startAction);
		toolBar.add(startButton);

		toolBar.addSeparator();
		//health = new Vector();
		selectedTab = MONITOR_TAB;
		//violations = new Vector();
		//ListModel model = new ListModel(violations);
		diagnosticsList = new JList();
		//eventField = new JTextField();
		//toolBar.add(eventField);



		/*eventButton = new ImageLozengeButton(ImageLoader
				.load("import_30x30_white.png"), "Log", new Color(51, 102, 153),
				new Color(51, 153, 204), 4);
		eventButton.setLabelColor(Color.white);
		eventButton.setAction(eventAction);
		toolBar.add(eventButton);

		toolBar.addSeparator();
		 */

		/*


		ImageLozengeButton stopButton = new ImageLozengeButton(ImageLoader
				.load("cancel_white_30x30.png"), "Stop", new Color(90, 0, 0),
				new Color(160, 0, 0), 4);
		stopButton.setLabelColor(Color.white);
		stopButton.setAction(endAction);
		toolBar.add(stopButton);

		 */
		//b.setIcon(new ImageIcon(getImage("play.png",30)));
		contentPane.add(toolBar,BorderLayout.PAGE_START);

		contentPane.add(outputContainer, BorderLayout.CENTER);
		contentPane.add(completeListPanel, BorderLayout.WEST);
		outputContainer.getViewport().add(Box.createRigidArea(new Dimension(800,800)));

		setSize(800,600);
		outputContainer.getHorizontalScrollBar().addMouseListener(this);
		//roundedPanelMon = SlickerFactory.instance().createRoundedPanel(50, chartBackgroundColor);
		roundedPanelHealth = SlickerFactory.instance().createRoundedPanel(50, chartBackgroundColor);

		//roundedPanelMon.setLayout(new BoxLayout(roundedPanelMon, BoxLayout.LINE_AXIS));
		roundedPanelHealth.setLayout(new BoxLayout(roundedPanelHealth, BoxLayout.LINE_AXIS));

		roundedPanelDiag = SlickerFactory.instance().createRoundedPanel();		
		primo = true;
		//premviolLabel = new JLabel ("");
		//singleviolLabel = new JLabel ("");
		//globalviolLabel = new JLabel ("");
		//JPanel labelsPane = new JPanel();
		//labelsPane.setLayout(new BorderLayout());
		//labelsPane.add(premviolLabel,BorderLayout.NORTH);
		//labelsPane.add(singleviolLabel,BorderLayout.CENTER);
		//labelsPane.add(globalviolLabel,BorderLayout.SOUTH);
		//contentPane.add(labelsPane,BorderLayout.PAGE_END);
		/*setLocation(300, 300);
		this.setSize(300, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout(5, 5));

		requestButton = new JButton("SEND!");


		field = new JTextField();
		getContentPane().add(field, BorderLayout.PAGE_START);

		requestButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				processRequest(field.getText());
			}
		});
		getContentPane().add(requestButton,BorderLayout.PAGE_END);


		area = new JTextArea();
		getContentPane().add(new JScrollPane(area), BorderLayout.CENTER);


		operationalServiceProxy = new OperationalSupportClient(HOST, PORT);



		pack();
		setVisible(true);
		this.setLocationRelativeTo(null);
		//this.operationalServiceProxy = new OperationalSupportClient("localhost", PORT);
		//this.processRequest(null);
		//LoginDialog loginDialog = new LoginDialog(this);*/
	}

	//private void processRequest(String task) {
	//if (task != null) {
	//partialTrace.add(XEventUtils.generateEvent(task));
	//}
	//sendRequest();

	//partialTrace.setText(partialTrace());		
	//}

	protected void instancesSelectionChanged() {
		int[] selectedIndices = instancesList.getSelectedIndices();
		if(selectedIndices.length >0){
			selected = ((InstanceInfo)instances.get(selectedIndices[0])).getId();
			shipType = "REF MODEL: "+(String)types.get(selected);
			instancesContainer.revalidate();
			instancesContainer.repaint();
			roundedPanelHealth.removeAll();
			if((FluentChartStandardPanel)healthGraphs.get(selected)!= null){
				roundedPanelHealth.add((FluentChartStandardPanel)healthGraphs.get(selected));
			}
			if((Vector)violationsModels.get(selected)!= null){
				diagnosticsList.setModel(new ViolationListModel((Vector)violationsModels.get(selected)));
			}else{
				diagnosticsList.setModel(new ViolationListModel(new Vector()));
			}
			tpInstances = new SlickerTabbedPane(shipType, new Color(240, 240, 240, 230), new Color(0, 0, 0, 230),
					new Color(220, 220, 220, 150));			
			tpInstances.addTab("Monitor",roundedPanelHealth);
			tpInstances.addTab("Diagnostics", roundedPanelDiag);
			tpInstances.addMouseListener(this);
			if(selectedTab == HEALTH_TAB ){
				tpInstances.selectTab("Health Trend");
			}else if (selectedTab == MONITOR_TAB){
				tpInstances.selectTab("Monitor");
			}else{
				tpInstances.selectTab("Diagnostics");
			}
			outputContainer.getViewport().removeAll();
			outputContainer.getViewport().add(tpInstances);
			outputContainer.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			outputContainer.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		}
	}

	private void sendRequest() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ProxyDaemon daemon;
		try {
			daemon = new ProxyDaemon(4444, this);
			daemon.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



	public class ProxySimulator extends Thread
	{

		@SuppressWarnings("unused")
		private final Socket socket;
		private final OutputStream outStream;
		private final InputStream inStream;

		public ProxySimulator(final Socket socket, final OutputStream outStream, final InputStream inStream) {
			super("Proxy Simulator");
			setDaemon(true);
			this.outStream = outStream;
			this.inStream = inStream;
			this.socket = socket;
			types= new Hashtable();
		}
		@Override
		public void run()
		{
			BufferedReader in = null;
			HashMap properties = new HashMap();
			String referenceXML = null;
			PrintWriter out = null;
			String[] completeSetEvents = null;
			Hashtable handles = new Hashtable();
			partialTraces = new Hashtable();
			in = new BufferedReader(new InputStreamReader(inStream));
			out = new PrintWriter(outStream,true);
			String letto = null;
			SessionHandle handle = null;
			//File tempConf = null;
			File message = null;
			//File referenceModel = null;
			try {
			//	tempConf = File.createTempFile("conf", ".xml");
			//	tempConf.deleteOnExit();
			//	PrintWriter printInTemp = new PrintWriter(new FileWriter(tempConf));
				//	referenceModel = File.createTempFile("model", ".xml");
				//	referenceModel.deleteOnExit();
				//		PrintWriter printreferenceModel = new PrintWriter(new FileWriter(referenceModel));
				referenceXML = in.readLine();
				System.out.println(referenceXML);
			//	while(!referenceXML.contains("</model>")){
			//		referenceXML = referenceXML + in.readLine();
			//	}
				
				
				
				Vector weights = new Vector();
				letto = in.readLine();
				while(!letto.equals("END_WEIGHTS")){
					System.out.println(letto);
					weights.add(letto);
					letto = in.readLine();
				}
				int timeWindowSize = 1;
				
				properties.put("weights", weights);
				properties.put("timeWindow", timeWindowSize);
				monitor.accept(referenceXML,weights,timeWindowSize);
			//	if(!letto.startsWith("<")){
			//		letto = letto.substring(letto.indexOf("<"));
			//	}
//				while(!letto.endsWith("</configuration>")){
//					printInTemp.println(letto);
//					System.out.println(letto);
//					letto  = in.readLine();
//				}
		//		printInTemp.println(letto);
		//		System.out.println(letto);
		//		printInTemp.flush();
		//		printInTemp.close();
				//	printreferenceModel.flush();
				//	printreferenceModel.close();
				//completeSetEvents = letto.split(",");
				completeSetEvents = new String[0];
				message = File.createTempFile("message", ".xml");
				message.deleteOnExit();
				PrintWriter printmessage = new PrintWriter(new FileWriter(message));
				letto = in.readLine();
				System.out.println(letto);

				while(!letto.contains("</Entry>")){
					letto = in.readLine();	
					System.out.println(letto);
					printmessage.println(letto);
				}
				//printmessage.println(letto);
				printmessage.flush();
		//		printmessage.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			System.out.println("MESSAGGIO: "+message);
			while(message != null){
				try {
					SAXBuilder parserSax = new SAXBuilder();
			//		File configuration = tempConf;
				//	org.jdom.Document doc = parserSax.build(configuration);	
					org.jdom.Document mess = parserSax.build(message);
				//	Element rootConf = doc.getRootElement();
					Element rootMess = mess.getRootElement();
					//Element models = root.getChild("model");
					String modelID = rootMess.getChild("ModelID").getText();
					String processInstanceID = rootMess.getChild("ProcessInstanceID").getText();			
					types.put(processInstanceID, modelID);
					if(!handles.containsKey(processInstanceID)){
						//boolean found = false;
						//	Element model = null;
						//	for(Object modelObj : models.getChildren()){
						//		model = (Element) modelObj;
						//		if(modelID.equals(model.getChild("id").getText())){
						//		path = model.getChild("path").getText();		
						//			found = true;
						//		}
						//	}								
						//	if(!found){
						//		path = models.getChild("default").getChild("path").getText();
						//	}
					//	referenceModel = File.createTempFile("model", ".xml");
					//	referenceModel.deleteOnExit();								
						//Documento 


					//	org.jdom.Document document = new org.jdom.Document();
					//	Element el = rootConf.getChild("inline").getChild("model");
					//	el.detach();
					//	document.addContent(el);
						//Creazione dell'oggetto XMLOutputter 
					//	XMLOutputter outputter = new XMLOutputter(); 
						//Imposto il formato dell'outputter come "bel formato" 
					//	outputter.setFormat(Format.getPrettyFormat()); 
						//Produco l'output sul file xml.foo 
					//	outputter.output(document, new FileOutputStream(referenceModel)); 
					//	System.out.println("File creato:"); 
						//Stampo l'output anche sullo standard output 
					//	outputter.output(document, System.out); 

	//	broker = XMLBrokerFactory.newAssignmentBroker(referenceModel.getAbsolutePath());

						
						
						
						handle = SessionHandle.create("localhost",1202, DeclareMonitorQuery.INSTANCE,properties);
						handle.setModel(DeclareLanguage.INSTANCE, referenceXML);
						handles.put(processInstanceID, handle);
					}else{
						
						handle = (SessionHandle)handles.get(processInstanceID);
						//handle.setModel(DeclareLanguage.INSTANCE, referenceXML);
					}
					outputContainer.getHorizontalScrollBar().setValue(value);
					System.out.println("RICEVUTO DA POSEIDON "+ letto);
					XTrace partialTrace;
					System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
							"com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

					String eventName = rootMess.getChild("WorkflowModelElement").getText();
					long timestamp = new Long(rootMess.getChild("Timestamp").getText()).longValue();
					String piID = rootMess.getChild("ProcessInstanceID").getText();

					if (partialTraces.containsKey(piID)) {
						partialTrace = (XTrace) partialTraces.get(piID);
					} else {
						partialTrace = new XTraceImpl(new XAttributeMapImpl());
						if (!instancesAll.contains(piID)) {
							instancesAll.add(piID);
						}
					}
					
					//System.out.println("non Lo contiene: "+shType);
					converter = new FluentsConverter(new NoGroupingStrategy());
					XConceptExtension.instance().assignName(partialTrace, piID);
					boolean done = false;
					if(eventName.equals("complete")){
						done = true;
					}
					XEvent completeEvent = new XEventImpl();
					XTimeExtension.instance().assignTimestamp(completeEvent, timestamp);
					XConceptExtension.instance().assignName(completeEvent, eventName/*.toLowerCase()*/);
					System.out.println("SENDING EVENT: "+XConceptExtension.instance().extractName(completeEvent));
					handle.addEvent(completeEvent);
					partialTrace.add(completeEvent);
					partialTraces.put(piID, partialTrace);
					XTraceImpl sentEvents = new XTraceImpl(new XAttributeMapImpl());
					XConceptExtension.instance().assignName(sentEvents, piID);
					sentEvents.add(completeEvent);
					XLog emptyLog = XFactoryRegistry.instance().currentDefault().createLog();
					//System.out.println("sending "+(d.getElementsByTagName("ProcessInstanceID")).item(0).getTextContent()+";"+shType+"at "+System.currentTimeMillis());
					System.out.println("PARTIAL TRACE ID: "+XConceptExtension.instance().extractName(partialTrace));
					System.out.println("PARTIAL TRACE LENGTH: "+partialTrace.size());
					emptyLog.add(sentEvents);
					
					Hashtable r = monitor.simple(emptyLog, piID, done);
					
					analysis = r;
					
					boolean viol = false;
					double health = 1;//((Double)analysis.get("health")).doubleValue();
					healthHash.put(timestamp,health);
					if (health < 1) {
						if (!instancesViol.contains(piID)) {
							instancesViol.add(piID);
						}
					} else {
						if (instancesViol.contains(piID)) {
							instancesViol.remove(piID);
						}
					}										
					if(showA){
						instances = new Vector();
						for(int i=0; i<instancesAll.size();i++){
							InstanceInfo ii = new InstanceInfo();
							ii.setId((String)instancesAll.get(i));
							if(instancesViol.contains(instancesAll.get(i))){
								ii.setViolated(true);
							}else{
								ii.setViolated(false);
							}
							instances.add(ii);
						}
					}else if (showV){
						instances = new Vector();
						for(int i=0; i<instancesViol.size();i++){
							InstanceInfo ii = new InstanceInfo();
							ii.setId((String)instancesViol.get(i));
							ii.setViolated(true);
							instances.add(ii);
						}
					}else if(searching){
						instances = new Vector();
						for(int i=0; i<instancesAll.size();i++){
							if(((String)instancesAll.get(i)).startsWith(searchField.getText())){
								InstanceInfo ii = new InstanceInfo();
								ii.setId((String)instancesAll.get(i));
								if(instancesViol.contains(instancesAll.get(i))){
									ii.setViolated(true);
								}else{
									ii.setViolated(false);
								}
								instances.add(ii);
							}
						}
					}
					int[] selectedIndices = instancesList.getSelectedIndices();
					int index = -1;
					if(selectedIndices.length>0){
						index = selectedIndices[0];
					}
					instancesList.clearSelection();
					instancesList.setModel(new ListModel(instances));
					instancesList.setSelectedIndex(index);
					out.println(((Math.rint(health)	* Math.pow(10, 2)) / Math.pow(10, 2)));				
					if(tpInstances!=null){
						if(tpInstances.getSelected()!=null){
							if(tpInstances.getSelected().equals(roundedPanelHealth)){
								selectedTab = HEALTH_TAB;
							}else{
								selectedTab = DIAGNOSTICS_TAB;
							}
						}
					}
					Vector vv = (Vector)violationsModels.get(piID);
					String diags = "Observed   "+eventName;
					int oldSize;
					if(vv==null){
						oldSize = 0;
					}else{
						oldSize = vv.size();
					}
					updateOutput(piID,eventName,completeSetEvents);
					outputContainer.getHorizontalScrollBar().setValue(value);
					vv = (Vector)violationsModels.get(piID);
					int newSize;
					if(vv==null){
						newSize = 0;
					}else{
						newSize = vv.size();
					}
					if(oldSize==newSize){
						out.println("");
					}else{
						String positive = (String)analysis.get("positive");
						if (!positive.isEmpty()) {
							if (positive != null) {
								String[] positivesForSize = positive.split(",");
								if (positivesForSize.length == 1) {
									diags = diags + ",   while for this reference model expecting   "
											+ positivesForSize[0];
								} else {
									String posList = positive.replace(",", ", ");
									int indCo = posList.lastIndexOf(",");
									String sub = posList.substring(indCo);
									posList = posList
											.replaceFirst(sub, sub.replaceFirst(", ", ", or "));
									diags = diags + ",   while for this reference model expecting   "
											+ posList;
								}
							}
						}
						String[] negativeSet = null;
						String negative = (String)analysis.get("negative");
						if (!negative.isEmpty()) {
							if (negative != null) {
								negativeSet = negative.split(",");
							}
						}
						String[] positiveSet = positive.split(",");
						Vector ve = new Vector();
						for(int i=0;i<positiveSet.length;i++){
							ve.add(positiveSet[i]);
						}
						if (negativeSet != null) {	
							if (negativeSet.length >= 1) {
								Vector vecCom = new Vector();
								for (int g = 0; g < completeSetEvents.length; g++) {
									vecCom.add(completeSetEvents[g]);
								}
								Vector vecNeg = new Vector();
								for (int g = 0; g < negativeSet.length; g++) {
									vecNeg.add(negativeSet[g]);
								}
								for (int g = 0; g < completeSetEvents.length; g++) {
									if (vecNeg.contains(completeSetEvents[g])) {
										vecCom.remove(completeSetEvents[g]);
									}
								}
								diags = diags + "   (or otherwise everything different from   ";
								String last = null;
								if (vecNeg.size() == 1) {
									diags = diags + vecNeg.get(0)+ ")";
								} else {
									for (int g = 0; g < vecNeg.size(); g++) {
										if(!ve.contains(vecNeg.get(g))){
											diags = diags + vecNeg.get(g) + ", ";
											last = (String)vecNeg.get(g);
										}
									}

									diags = diags.replace(", "+last + ", ", " and " + last + ")");

								}
							}
						}

						out.println(diags);
						//	p.flush();
					}
					if(selected.equals(piID)){
						roundedPanelHealth.removeAll();
						roundedPanelHealth.add((FluentChartStandardPanel)healthGraphs.get(selected));
						diagnosticsList.setModel(new ViolationListModel((Vector)violationsModels.get(selected)));
					}
					outputContainer.getHorizontalScrollBar().setValue(value);

				} catch (Exception e) {
					e.printStackTrace();

				}

				try {
					message.delete();
					message = File.createTempFile("message", ".xml");
					message.deleteOnExit();
					PrintWriter printmessage = new PrintWriter(new FileWriter(message));
					letto = in.readLine();
					if(letto==null){
						message = null;
					}else{
						while(!letto.contains("/Entry>")){							
							printmessage.println(letto);
						}
						printmessage.println(letto);
						printmessage.flush();
						printmessage.close();
					}
					outputContainer.getHorizontalScrollBar().setValue(value);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
		}

		// We have to actually update the model values in the event thread
		// to make sure that they are consistent. This is a quick operation
		// becuase all we are doing is updating the references to the data arrays.


	}

	private void updateOutput(String piID, String eventName, String[] completeSetEvents) {
		try {
			XTrace trace = (XTrace) partialTraces.get(piID);
			boolean timestampPrinted = false;
			double healthValue;
			Vector healthvec;
			if (healths.containsKey(piID)) {
				healthvec = (Vector) healths.get(piID);
			} else {
				healthvec = new Vector();
			}
			healthvec.add(1);
			healths.put(piID, healthvec);
			int count = 1;
			//boolean stopped = (Boolean)analysis.get("isStopped");
			count++;
			Vector violations;
			//if(!stopped){

			if ((analysis.get("prematureCompletion") != null) || (analysis.get("single") != null) || (analysis.get("global") != null))  {
				if (analysis.get("prematureCompletion") != null) {
					if (violationsModels.containsKey(piID)) {
						violations = (Vector) violationsModels.get(piID);
					} else {
						violations = new Vector();
					}
					timestampPrinted = true;
					violations.add("AT " + (XExtendedEvent.wrap(trace.get(trace.size() - 1))).getTimestamp());
					violations.add(analysis.get("prematureCompletion"));

					violationsModels.put(piID, violations);

				}
				if (analysis.get("single") != null) {
					//	roundedPanelMon.setBackground(Color.red);

					if (violationsModels.containsKey(piID)) {
						violations = (Vector) violationsModels.get(piID);
					} else {
						violations = new Vector();
					}
					timestampPrinted = true;
					//singleviolLabel.setText((String)viol.get("single"));
					violations.add("AT " + (XExtendedEvent.wrap(trace.get(trace.size() - 1))).getTimestamp());
					violations.add(analysis.get("single"));

					violationsModels.put(piID, violations);

				}

				if (analysis.get("global") != null) {
					//	roundedPanelMon.setBackground(Color.red);
					if (violationsModels.containsKey(piID)) {
						violations = (Vector) violationsModels.get(piID);
					} else {
						violations = new Vector();
					}
					//globalviolLabel.setText((String)viol.get("global"));
					if (timestampPrinted == false) {
						violations.add("AT " + (XExtendedEvent.wrap(trace.get(trace.size() - 1))).getTimestamp());
					}

					String diags = "Observed   " + eventName;

					String positive = (String)analysis.get("positive");
					if (!positive.isEmpty()) {
						if (positive != null) {
							String[] positivesForSize = positive.split(",");
							if (positivesForSize.length == 1) {
								diags = diags + ",   while for this reference model expecting   " + positivesForSize[0];
							} else {
								String posList = positive.replace(",", ", ");
								int indCo = posList.lastIndexOf(",");
								String sub = posList.substring(indCo);
								posList = posList.replaceFirst(sub, sub.replaceFirst(", ", ", or "));
								diags = diags + ",   while for this reference model expecting   " + posList;
							}
						}
					}

					String[] negativeSet = null;
					String negative = (String)analysis.get("negative");
					if (!negative.isEmpty()) {
						if (negative != null) {
							negativeSet = negative.split(",");
						}
					}
					String[] positiveSet = positive.split(",");
					Vector ve = new Vector();
					for(int i=0;i<positiveSet.length;i++){
						ve.add(positiveSet[i]);
					}
					if (negativeSet != null) {	
						if (negativeSet.length >= 1) {
							Vector vecCom = new Vector();
							for (int g = 0; g < completeSetEvents.length; g++) {
								vecCom.add(completeSetEvents[g]);
							}
							Vector vecNeg = new Vector();
							for (int g = 0; g < negativeSet.length; g++) {
								vecNeg.add(negativeSet[g]);
							}
							for (int g = 0; g < completeSetEvents.length; g++) {
								if (vecNeg.contains(completeSetEvents[g])) {
									vecCom.remove(completeSetEvents[g]);
								}
							}
							diags = diags + "   (or otherwise everything different from   ";
							String last = null;
							if (vecNeg.size() == 1) {
								diags = diags + vecNeg.get(0)+ ")";
							} else {
								for (int g = 0; g < vecNeg.size(); g++) {
									if(!ve.contains(vecNeg.get(g))){
										diags = diags + vecNeg.get(g) + ", ";
										last = (String)vecNeg.get(g);
									}
								}

								diags = diags.replace(", "+last + ", ", " and " + last + ")");

							}
						}
					}

					//violations.add((String)viol.get("global"));

					violations.add(diags);
					violationsModels.put(piID, violations);

				}
			}
			//violLabel.setForeground(Color.red);	  */				
			String fluent = (String)analysis.get("fluents");

			System.out.println(fluent);
			HealthCountingMetric metric = new HealthCountingMetric(healthHash);
			FluentsModel model = converter.toFluentsModel(fluent);
			Xes2RecTraceTranslator traceTranslator = new Xes2RecTraceTranslator(
					it.unibo.ai.rec.common.TimeGranularity.MILLIS, Xes2RecTraceTranslator.TimestampStrategy.ABSOLUTE);
			RecTrace rtrace = traceTranslator.translate(trace);
			//FluentChartContainer hPanel = new HealthChartStandardPanel(factory,healthvec, trace, null);

			chartPanel = new FluentChartStandardPanel(factory,metric);
			chartPanel.update(rtrace, model);
			//hPanel.update(rtrace, model);
			chartPanel.getChartPanel().setOpaque(true);
			chartPanel.getChartPanel().setBackground(Color.white);

			roundedPanelDiag.setLayout(new BorderLayout());
			//roundedPanelMon.add(chartPanel);
			if(primo){		
				roundedPanelHealth.add(chartPanel);
				primo = false;
			}
			//diagnosticsList.setSize(roundedPanelDiag.getWidth(), roundedPanelDiag.getHeight());
			roundedPanelDiag.add(diagnosticsList, BorderLayout.NORTH);
			chartPanel.setAlignmentY(Component.TOP_ALIGNMENT);
			monitorGraphs.put(piID, chartPanel);
			healthGraphs.put(piID, chartPanel);
			if(selected.equals(piID)){
				tpInstances = new SlickerTabbedPane(shipType, new Color(240, 240, 240, 230), new Color(0, 0, 0, 230),
						new Color(220, 220, 220, 150));			
				tpInstances.addTab("Monitor",roundedPanelHealth);
				//tpInstances.addTab("Health Trend", roundedPanelHealth);
				tpInstances.addTab("Diagnostics", roundedPanelDiag);
				tpInstances.addMouseListener(this);
				if(selectedTab == HEALTH_TAB ){
					tpInstances.selectTab("Health Trend");
				}else if (selectedTab == MONITOR_TAB){
					tpInstances.selectTab("Monitor");
				}else{
					tpInstances.selectTab("Diagnostics");
				}
				outputContainer.getViewport().removeAll();
				outputContainer.getViewport().add(tpInstances);
				//outputContainer.getViewport().add(roundedPanel2);
				//System.out.println("CONTAINER DIMENSIONS: "+roundedPanelMon.getSize()+", PREF: "+roundedPanelMon.getPreferredSize());
				//outputContainer.getHorizontalScrollBar().setValue(roundedPanel2.getPreferredSize().width);
				outputContainer.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				outputContainer.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			}
			outputContainer.getHorizontalScrollBar().setValue(value);
			//	instancesContainer = 
			//outputContainer.getVerticalScrollBar().setValue(roundedPanel.getPreferredSize().height);
			/*}else{
				if (!stopped) {
					if (violationsModels.containsKey(piID)) {
						violations = (Vector) violationsModels.get(piID);
					} else {
						violations = new Vector();
					}
					violations.add("AT " + (XExtendedEvent.wrap(trace.get(trace.size() - 1))).getTimestamp());
					violations.add((String) analysis.get("global"));
					violationsModels.put(piID, violations);
				} else {
					if (violationsModels.containsKey(piID)) {
						violations = (Vector) violationsModels.get(piID);
					} else {
						violations = new Vector();
					}
					violations.add("AT " + (XExtendedEvent.wrap(trace.get(trace.size() - 1))).getTimestamp());
					violations.add((String) analysis.get("prematureCompletion"));
					violationsModels.put(piID, violations);
				}
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}




	public static void main(String[] args) {
		try {
			if(args.length>0){
				new MoBuConClient(args[0]).setVisible(true);
			}else{
				new MoBuConClient("conf.xml").setVisible(true);
			}
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void actionPerformed(ActionEvent ae) {
		String action = ae.getActionCommand();
		// Per actioncommand do an action:
		if (action.equals("showViolated")) {
			showV = true;
			showA = false;
			searching = false;

			int ind= -1;
			instances = new Vector();
			for(int i=0; i<instancesViol.size();i++){
				InstanceInfo ii = new InstanceInfo();
				ii.setId((String)instancesViol.get(i));
				ii.setViolated(true);
				instances.add(ii);
				if(ii.getId().equals(selected)){
					ind= i;
				}
			}
			instancesList.setModel(new ListModel(instances));
			instancesList.setSelectedIndex(ind);
			search.setEnabled(false);
		} else if (action.equals("showAll")) {
			showV = false;
			showA = true;
			searching = false;
			instances = new Vector();
			int ind = -1;
			for(int i=0; i<instancesAll.size();i++){
				InstanceInfo ii = new InstanceInfo();
				ii.setId((String)instancesAll.get(i));
				if(instancesViol.contains(instancesAll.get(i))){
					ii.setViolated(true);
				}else{
					ii.setViolated(false);
				}
				instances.add(ii);
				if(ii.getId().equals(selected)){
					ind= i;
				}

			}
			instancesList.setModel(new ListModel(instances));
			instancesList.setSelectedIndex(ind);
			search.setEnabled(false);

		} else if (action.equals("search")){ 
			//search.setEnabled(true);
			showV = false;
			showA = false;
			searching = true;

			int ind = -1;
			instances = new Vector();
			for(int i=0; i<instancesAll.size();i++){
				if(((String)instancesAll.get(i)).startsWith(searchField.getText())){
					InstanceInfo ii = new InstanceInfo();
					ii.setId((String)instancesAll.get(i));
					if(instancesViol.contains(instancesAll.get(i))){
						ii.setViolated(true);
					}else{
						ii.setViolated(false);
					}
					instances.add(ii);
					if(ii.getId().equals(selected)){
						ind= i;
					}
				}
			}
			instancesList.setModel(new ListModel(instances));

			instancesList.setSelectedIndex(ind);


		} else if (action.equals("searching")){
			showV = false;
			showA = false;
			searching = true;

		}  
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		if(arg0.getSource() instanceof JTextField){
			searchRadioButton.setSelected(true);
			showV = false;
			showA = false;
			searching = true;
		}

	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		if(tpInstances!=null){
			if(tpInstances.getSelected()!=null){
				if(tpInstances.getSelected().equals(roundedPanelHealth)){
					selectedTab = HEALTH_TAB;
					//	}else if (tpInstances.getSelected().equals(roundedPanelMon)){
					//	selectedTab= MONITOR_TAB;
				}else{
					selectedTab = DIAGNOSTICS_TAB;
				}
			}
		}

	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		if(tpInstances!=null){
			if(tpInstances.getSelected()!=null){
				if(tpInstances.getSelected().equals(roundedPanelHealth)){
					selectedTab = HEALTH_TAB;
					//}else if (tpInstances.getSelected().equals(roundedPanelMon)){
					//selectedTab= MONITOR_TAB;
				}else{
					selectedTab = DIAGNOSTICS_TAB;
				}
			}
		}

	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		if(tpInstances!=null){
			if(tpInstances.getSelected()!=null){
				if(tpInstances.getSelected().equals(roundedPanelHealth)){
					selectedTab = HEALTH_TAB;
					//}else if (tpInstances.getSelected().equals(roundedPanelMon)){
					//	selectedTab= MONITOR_TAB;
				}else{
					selectedTab = DIAGNOSTICS_TAB;
				}
			}
		}

	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		if(arg0.getSource() instanceof JScrollBar){
			value = outputContainer.getHorizontalScrollBar().getValue();
		}
		if(tpInstances!=null){
			if(tpInstances.getSelected()!=null){
				if(tpInstances.getSelected().equals(roundedPanelHealth)){
					selectedTab = HEALTH_TAB;
					//}else if (tpInstances.getSelected().equals(roundedPanelMon)){
					//selectedTab= MONITOR_TAB;
				}else{
					selectedTab = DIAGNOSTICS_TAB;
				}
			}
		}

	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		int ind= -1;
		instances = new Vector();
		for(int i=0; i<instancesAll.size();i++){
			if(((String)instancesAll.get(i)).startsWith(searchField.getText())){
				InstanceInfo ii = new InstanceInfo();
				ii.setId((String)instancesAll.get(i));
				if(instancesViol.contains(instancesAll.get(i))){
					ii.setViolated(true);
				}else{
					ii.setViolated(false);
				}
				instances.add(ii);
				if(ii.getId().equals(selected)){
					ind= i;
				}
			}
		}
		instancesList.setModel(new ListModel(instances));
		instancesList.setSelectedIndex(ind);
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void finalize(){
		try {
			Set keys = handles.keySet();
			Iterator it = keys.iterator();
			while(it.hasNext()){
				((SessionHandle<Object, Map<String, Object>, String, Object, Object>)handles.get(it.next())).close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}


