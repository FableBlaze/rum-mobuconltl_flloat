package nl.tue.mobucon.client;

import java.io.File;
import java.io.PrintWriter;
import java.net.Socket;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.in.XMxmlGZIPParser;
import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.in.XesXmlGZIPParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;

public class LDLReplayer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String host = "localhost";
		//String model = "begin,pay,acc,cancel,get,return,complete#(<>(return))#ltl";
	
		//Alfabeto
		String model = "begin,pay,acc,cancel,get,return,tau,complete#"
		// Primo metaconstraint (funziona)	
		//	    +"(!(<(true)*>((<pay>(tt)) && (!([true](ff)))))) || (<(true)*>((<acc>(tt)) && (!([true](ff))))),"
		// Secondo metaconstraint (funziona)			
		//		+ "!((<(true)*>((<get>(tt)) && (!([true](ff))))) && (<(true)*>((<cancel>(tt)) && (!([true](ff)))))),"
        // Terzo metaconstraint  (funziona)				
		//		+ "[(!(pay))* ; pay; (!(acc))*]!get"
		// Quarto metaconstraint (funziona)		
				//+ "(((<(true)*>((<get>(tt)) && (!([true](ff))))) && (<(true)*>((<cancel>(tt)) && (!([true](ff)))))))->(<((tau || begin || pay || acc)* ; get ; (!(cancel))* ; cancel ; (true)*) + ((tau || begin || pay || acc)* ; cancel ; !(get)* ; get ; (true)*)>(<(((<(true)?>(tt))?) ; (true))*>(<(return)?>(tt)))),"
		// Quinto metaconstraint:
		//		+ "(<( (tau || begin || return || acc || get)* ; pay ; (tau || begin || return || acc || get || pay)* ; cancel)>(tt))  && (  <( (tau || begin || return || acc || get)* ; pay ; (tau || begin || return || acc || get || pay)* ; cancel)>(tt) -> (<(!((<(true)*>((<get>(tt)) && (!([true](ff))))) && (<(true)*>((<cancel>(tt)) && (!([true](ff)))))))>))"
//				+ "+ ( (tau || begin || return || acc || complete)* ; cancel ; (tau || begin || return || acc || complete || canel)* ; pay ; (tau || begin || return || acc || complete || pay); !(<(true)*>((<get>(tt)) && (!([true](ff))))))>(tt)"
		// Sesto metaconstraint:
//				+ "(<( (tau || begin || return || acc || get)* ; pay ; (tau || begin || return || acc || get || pay)* ; cancel ; (tau || begin || return || acc || pay || cancel)*   ) + ( (tau || begin || return || acc)* ; cancel ; ( (tau || begin || return || acc) || cancel)* ; pay ; (!get)* )>(tt)) -> (!((<(true)*>((<get>(tt)) && (!([true](ff))))) && (<(true)*>((<cancel>(tt)) && (!([true](ff)))))))"
			//	+ "#ldl#<>pay -> <>acc,!(<>get /\\ <>cancel),Contextual absence: get task forbidden while <>pay -> <>acc is possibly violated,Reactive compensation: permanent violation of !(<>get /\\ <>cancel) compensated by a consequent <>return,Conflict: presence of a conflict for !(<>get /\\ <>cancel) and [](pay -> O<>get),Preference: preference of !(<>get /\\ <>cancel) over [](pay -> O<>get) in case a conflict is ever encountered";		
				+ "!((<>get))"
				+ "#ltl#C3";
		String inputLogFileName = "./Tosem.xes";
		try{
			XLog log = null;
			if(inputLogFileName.toLowerCase().contains("mxml.gz")){
				XMxmlGZIPParser parser = new XMxmlGZIPParser();
				if(parser.canParse(new File(inputLogFileName))){
					try {
						log = parser.parse(new File(inputLogFileName)).get(0);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else if(inputLogFileName.toLowerCase().contains("mxml")){
				XMxmlParser parser = new XMxmlParser();
				if(parser.canParse(new File(inputLogFileName))){
					try {
						log = parser.parse(new File(inputLogFileName)).get(0);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			if(inputLogFileName.toLowerCase().contains("xes.gz")){
				XesXmlGZIPParser parser = new XesXmlGZIPParser();
				if(parser.canParse(new File(inputLogFileName))){
					try {
						log = parser.parse(new File(inputLogFileName)).get(0);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else if(inputLogFileName.toLowerCase().contains("xes")){
				XesXmlParser parser = new XesXmlParser();
				if(parser.canParse(new File(inputLogFileName))){
					try {
						log = parser.parse(new File(inputLogFileName)).get(0);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			@SuppressWarnings("resource")
			Socket socket = new Socket (host,4444);
			socket.isConnected();
			PrintWriter writeOnTheSocket = new PrintWriter(socket.getOutputStream(),true);
			writeOnTheSocket.println(model);
			writeOnTheSocket.flush();
	
			double[] weights = new double[]{1.,1.,1.,1.,1.,1.};
			for(int i = 0; i<weights.length; i++){
				writeOnTheSocket.println(weights[i]);
				writeOnTheSocket.flush();
			}
			writeOnTheSocket.println("END_WEIGHTS");
			writeOnTheSocket.flush();
			writeOnTheSocket.println(5);
			writeOnTheSocket.flush();
		   int i = 0;
			for(XTrace trace : log){
				for(XEvent event: trace){
					String line = "<?xml version=\"1.0\"?><Entry><ProcessID>0</ProcessID><ProcessInstanceID>"+XConceptExtension.instance().extractName(trace)+"</ProcessInstanceID><ModelID>Municipalities</ModelID><WorkflowModelElement>"+XConceptExtension.instance().extractName(event)+"</WorkflowModelElement><Timestamp>"+XTimeExtension.instance().extractTimestamp(event).getTime()+"</Timestamp><EventType>complete</EventType></Entry>";
					System.out.println(line);
					writeOnTheSocket.println(line);
					writeOnTheSocket.flush();
					if (i==0){
						Thread.sleep(10000);
					}else{
						Thread.sleep(10000);
					}
					i++;
				}
			}
			writeOnTheSocket.close();
		}catch (Exception e){e.printStackTrace();
		}
	}
}
