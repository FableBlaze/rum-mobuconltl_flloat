package nl.tue.mobucon.client;

import info.clearthought.layout.TableLayout;
import it.unibo.ai.rec.common.TimeGranularity;
import it.unibo.ai.rec.engine.FluentsConverter;
import it.unibo.ai.rec.model.FluentsModel;
import it.unibo.ai.rec.model.NoGroupingStrategy;
import it.unibo.ai.rec.model.RecTrace;
import it.unibo.ai.rec.visualization.BasicDateEventOutputter;
import it.unibo.ai.rec.visualization.FluentChartContainer;
import it.unibo.ai.rec.visualization.FluentChartFactory;
import it.unibo.ai.rec.visualization.FluentChartStandardPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import org.deckfour.uitopia.ui.components.ImageLozengeButton;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XExtendedEvent;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.in.XMxmlGZIPParser;
import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.in.XesXmlGZIPParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XEventImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.DOMOutputter;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jfree.data.general.AbstractDataset;
import org.processmining.operationalsupport.client.InvocationException;
import org.processmining.operationalsupport.client.SessionHandle;
import org.processmining.operationalsupport.messages.reply.ResponseSet;
import org.processmining.plugins.declareminer.visualizing.AssignmentModel;
import org.processmining.plugins.declareminer.visualizing.AssignmentViewBroker;
import org.processmining.plugins.declareminer.visualizing.ConstraintDefinition;
import org.processmining.plugins.declareminer.visualizing.Parameter;
import org.processmining.plugins.declareminer.visualizing.XMLBrokerFactory;
import org.processmining.plugins.mobuconltl.DeclareLanguage;
import org.processmining.plugins.mobuconltl.DeclareMonitorQuery;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

import com.fluxicon.slickerbox.colors.SlickerColors;
import com.fluxicon.slickerbox.components.SlickerTabbedPane;
import com.fluxicon.slickerbox.factory.SlickerDecorator;
import com.fluxicon.slickerbox.factory.SlickerFactory;
import com.thoughtworks.xstream.XStream;

public class LogReplayer extends JFrame{



	/**
	 * 
	 */
	//public static final String DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss.SSS+kk:zz";
	//public static final String DATE_FORMAT = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'+'K";
	private int value;
	public static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss:S";
	protected FluentsConverter converter;
	private FluentChartFactory factory;
	private static final long serialVersionUID = 1561407447457027863L;
	private JTextField  windSize;
	private JPanel contentPane;
	private AssignmentViewBroker broker = null;
	private String brokerFilePath;
	public static final String CASE_COMPLETE = "case_complete";
	private static final int MONITOR_TAB = 0;
	private static final int HEALTH_TAB = 1;
	private static final int DIAGNOSTICS_TAB = 2;
	private Color backgroundColor = SlickerColors.COLOR_BG_4;
	private Color chartBackgroundColor = new Color(232,232,232);
	private JRadioButton twentyfour;
	private JRadioButton sixty;
	private JRadioButton twelve;
	private JRadioButton six;
	private JRadioButton three;
	private JButton search;
	private String shipType = "REF MODEL: no model selected";
	//private static final String path = "/it/unibo/ai/rec/client/icons/";
	private int selectedTab;
	private boolean showV;
	private boolean showA;
	private boolean searching;
	//	private String confPath;
	private JCheckBox check;
	private Map<String, Object> analysis;
	boolean primo = true;
	FluentChartContainer chartPanel;

	private HashMap<String, Double> weights;
	private JTextField searchField;
	private Action startAction;
	//private Action endAction;
	//private Action eventAction;
	//	private JTextField eventField;
	private JScrollPane outputContainer;
	private JScrollPane instancesContainer;
	//private JPanel roundedPanelMon;
	private JPanel roundedPanelHealth;
	private JPanel roundedPanelDiag;
	private JPanel completeListPanel;
	private JPanel filterPanel;
	private JPanel searchPanel;
	//private JLabel premviolLabel;
	//private JLabel singleviolLabel;
	//private JLabel globalviolLabel;
	//private Vector violations;
	private JList diagnosticsList;
	//	private ImageLozengeButton eventButton;
	//private XLog log;
	//	private Vector infos;
	protected XLog log = null;
	protected JList instancesList;

	//	private boolean busy = false;

	@SuppressWarnings("serial")
	private void initActions() {
		startAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//	initLog();
				setEnabled(false);
				//		endAction.setEnabled(true);
				//	eventAction.setEnabled(true);
				//	resetTrace();
				//initLog(/*String path*/);
				//processRequest("start");

			}

		};
		startAction.setEnabled(true);

		/*	endAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				setEnabled(false);
				startAction.setEnabled(true);
				eventAction.setEnabled(false);
				//partialTrace.add(XEventUtils.generateEvent(CASE_COMPLETE));
				try {Thread.sleep(1000);}catch(Exception ex){}
				//		processRequest("complete");
			}
		};
		endAction.setEnabled(false);

		eventAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				//initLog(eventField.getText());
				//initLog();
				//Iterator tracesIterator = log.iterator();
				//	while(tracesIterator.hasNext()){
				//XTrace trace = (XTrace)tracesIterator.next();
				//Iterator eventIterator = trace.iterator();
				//while(eventIterator.hasNext()){
				//	XExtendedEvent ate = new XExtendedEvent((XEvent)eventIterator.next());
				//	String eventName = ate.getName();
				//	Date timeStamp = ate.getTimestamp();
				//	partialTrace.add((XEvent)eventIterator.next());
				//	processRequest("gigi");
				//	processRequest("a");
				//if(eventField.getText().length()>0){
				//processRequest(eventField.getText());
				//}
				//}
				//	}

		 */
		/*sendRequest();*/
		/*JFileChooser fileChooser = new JFileChooser();
				int n = fileChooser.showOpenDialog(OSPoseidonDeclareMonitorClient.this);
				File f = null;
				if(n!= JFileChooser.CANCEL_OPTION && n!= JFileChooser.ERROR_OPTION){
					f = fileChooser.getSelectedFile();
					eventField.setText(f.getAbsolutePath());
					initLog(f.getAbsolutePath());
					setEnabled(false);
					startAction.setEnabled(true);

					//eventAction.setEnabled(true);
					resetTrace();
				}
		 */
		//}
	};
	//	eventAction.setEnabled(false);

	//	}


	/*private void initLog(String path){
		File file = new File(path);
		String filename = file.getName();
		InputStream input;
		try {
			input = new FileInputStream(file);

			long fileSizeInBytes = file.length();
			XParser parser;
			if (filename.toLowerCase().endsWith(".xes") || filename.toLowerCase().endsWith(".xes.gz")) {
				parser = new XesXmlParser();
			} else {
				parser = new XMxmlParser();
			}
			Collection<XLog> logs = parser.parse(file);

			//parse(new XContextMonitoredInputStream(input, fileSizeInBytes, new Progress()));
			Response globalResponse = null;
			// log sanity checks;
			// notify user if the log is awkward / does miss crucial information
			if (logs.size() == 0) {
				throw new Exception("No processes contained in log!");
			}

			log = logs.iterator().next();
			XConceptExtension.instance().assignName(log, filename);

			if (log.isEmpty()) {
				throw new Exception("No process instances contained in log!");
			}



		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} */

	private JScrollPane createSlickerScrollPane() {

		JScrollPane scrollpane = new JScrollPane();
		scrollpane.setOpaque(false);
		scrollpane.getViewport().setOpaque(false);
		scrollpane.setBorder(BorderFactory.createEmptyBorder());
		//scrollpane.setViewportBorder(BorderFactory.createLineBorder(new Color(
		//		10, 10, 10), 2));
		scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		SlickerDecorator.instance().decorate(scrollpane.getVerticalScrollBar(),
				new Color(0, 0, 0, 0), new Color(140, 140, 140),
				new Color(80, 80, 80));
		scrollpane.getVerticalScrollBar().setOpaque(false);

		SlickerDecorator.instance().decorate(scrollpane.getHorizontalScrollBar(),
				new Color(0, 0, 0, 0), new Color(140, 140, 140),
				new Color(80, 80, 80));
		scrollpane.getHorizontalScrollBar().setOpaque(false);
		return scrollpane;
	}


	private JScrollPane createSlickerScrollPane(JList list) {


		JScrollPane scrollpane = new JScrollPane(list);
		scrollpane.setOpaque(false);
		scrollpane.getViewport().setOpaque(false);
		scrollpane.setBorder(BorderFactory.createEmptyBorder());
		//scrollpane.setViewportBorder(BorderFactory.createLineBorder(new Color(
		//		10, 10, 10), 2));
		scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		SlickerDecorator.instance().decorate(scrollpane.getVerticalScrollBar(),
				new Color(0, 0, 0, 0), new Color(140, 140, 140),
				new Color(80, 80, 80));
		scrollpane.getVerticalScrollBar().setOpaque(false);

		SlickerDecorator.instance().decorate(scrollpane.getHorizontalScrollBar(),
				new Color(0, 0, 0, 0), new Color(140, 140, 140),
				new Color(80, 80, 80));
		scrollpane.getHorizontalScrollBar().setOpaque(false);
		return scrollpane;
	}



	public LogReplayer(String confPath) throws JDOMException, IOException {
		setDefaultCloseOperation(EXIT_ON_CLOSE);	
		initActions();
		weights = new HashMap<String, Double>();
		instancesList = new JList();
		//outputContainer = createSlickerScrollPane();
		instancesContainer = createSlickerScrollPane();
		instancesContainer.setSize(200, 800);
		instancesContainer.setPreferredSize(new Dimension(200,800));
		instancesContainer.setMaximumSize(new Dimension(200,800));
		instancesContainer.setMinimumSize(new Dimension(200,800));

		final SlickerFactory factory = SlickerFactory.instance();


		JPanel buttonPanel = factory.createRoundedPanel();
		JButton loadMap = factory.createButton("LOAD MAP");
		loadMap.addMouseListener(new MouseListener() {



			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				JFileChooser jfc = new JFileChooser();
				jfc.showOpenDialog(contentPane);
				if(jfc.getSelectedFile()!=null){
					broker = XMLBrokerFactory.newAssignmentBroker(jfc.getSelectedFile().getAbsolutePath());
					brokerFilePath = jfc.getSelectedFile().getAbsolutePath();
					final Vector<String> list = new Vector<String>();
					AssignmentModel assmod = broker.readAssignment();
					double longest= 0.;
					for(ConstraintDefinition cd : assmod.getConstraintDefinitions()){
						String constraint = cd.getName()+"[";
						for(Parameter p : cd.getParameters()){
							if(cd.getBranches(p).iterator().hasNext()){
								constraint= constraint+cd.getBranches(p).iterator().next()+",";
							}
						}
						constraint = constraint.substring(0, constraint.length()-1)+"]";
						list.add(constraint);
						weights.put(constraint, 1.);
						if((double)constraint.length()>longest){
							longest = (double)constraint.length();
						}
					}
					double[] size = new double[list.size()+1];
					for(int i= 0; i<size.length; i++){
						size[i]= 20;
					}
					JLabel idLabel;
					JComboBox combo = null;
					SlickerFactory sf = SlickerFactory.instance();
					JPanel panel = sf.createRoundedPanel();
					panel.setLayout(new TableLayout(new double[][] { { 6.6 *longest, 50 }, size }));
					int i = 0;
					for(String constr : list){
						idLabel = sf.createLabel(constr);
						combo = sf.createComboBox(new Integer[]{1,2,3,4,5,6,7,8,9,10});
						final int j = i;
						//panel.add(bef, BorderLayout.NORTH);
						combo.addItemListener(new ItemListener() {

							@Override
							public void itemStateChanged(ItemEvent arg0) {
								weights.put(list.get(j), new Double(arg0.getItem().toString()));

							}
						});
						panel.add(idLabel, "0,"+i);
						panel.add(combo, "1,"+i);
						i++;
					}

					windSize = new JTextField("5");
					panel.add(windSize,"0,"+i);
					instancesContainer.setViewportView(panel);
				}else{
					((JButton)e.getSource()).setBackground(factory.createButton("").getBackground());
				}
				//WeightsListModel model = new WeightsListModel(list);
				//instancesList.setModel(model);
				//instancesList.setCellRenderer(new WeightsRenderer(longest));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});

		JButton loadLog = factory.createButton("LOAD LOG");
		loadLog.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent ev) {
				JFileChooser jfc = new JFileChooser();
				jfc.showOpenDialog(contentPane);
				if(jfc.getSelectedFile()!=null){
				String inputLogFileName = jfc.getSelectedFile().getAbsolutePath();

				
					if(inputLogFileName.toLowerCase().contains("mxml.gz")){
						XMxmlGZIPParser parser = new XMxmlGZIPParser();
						if(parser.canParse(new File(inputLogFileName))){
							try {
								log = parser.parse(new File(inputLogFileName)).get(0);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}else if(inputLogFileName.toLowerCase().contains("mxml")){
						XMxmlParser parser = new XMxmlParser();
						if(parser.canParse(new File(inputLogFileName))){
							try {
								log = parser.parse(new File(inputLogFileName)).get(0);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

					if(inputLogFileName.toLowerCase().contains("xes.gz")){
						XesXmlGZIPParser parser = new XesXmlGZIPParser();
						if(parser.canParse(new File(inputLogFileName))){
							try {
								log = parser.parse(new File(inputLogFileName)).get(0);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}else if(inputLogFileName.toLowerCase().contains("xes")){
						XesXmlParser parser = new XesXmlParser();
						if(parser.canParse(new File(inputLogFileName))){
							try {
								log = parser.parse(new File(inputLogFileName)).get(0);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

				}else{
					((JButton)ev.getSource()).setBackground(factory.createButton("").getBackground());
				}
				if(log == null){
					return;
				}



			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
		JButton startReplay = factory.createButton("START REPLAY");
		startReplay.addMouseListener(new MouseListener() {



			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				if(log==null){
					JOptionPane.showMessageDialog(contentPane, "LOG MISSING!");
					return;
				}
				if(broker==null){
					JOptionPane.showMessageDialog(contentPane, "MAP MISSING!");
					return;
				}
				String host = "localhost";
				try{
					long times=0;
					if(sixty.isSelected()){
						times = 1000;
					}else if(twentyfour.isSelected()){
						times = 2500;
					}else if(twelve.isSelected()){
						times = 5000;
					}else if(six.isSelected()){
						times = 10000;
					}else{
						times = 20000;
					}
					HashMap<Long,Vector<String>> events =  new HashMap<Long, Vector<String>>();
					Socket socket = new Socket (host,4444);
					socket.isConnected();
					PrintWriter writeOnTheSocket = new PrintWriter(socket.getOutputStream(),true);
					SAXBuilder sb = new SAXBuilder();
					DOMOutputter outputter = new DOMOutputter();
					org.jdom.Document xesDocument = sb.build(new File(brokerFilePath));
					org.w3c.dom.Document w3cdoc = outputter.output(xesDocument);

					writeOnTheSocket.println(getStringFromDoc(w3cdoc));
					writeOnTheSocket.flush();




					for(Double d : weights.values()){
						writeOnTheSocket.println(d);
						writeOnTheSocket.flush();
					}
					writeOnTheSocket.println("END_WEIGHTS");
					writeOnTheSocket.flush();
					writeOnTheSocket.println(windSize.getText());
					writeOnTheSocket.flush();
					Vector<Long> timestamps = new Vector<Long>();

					for(XTrace trace : log){
						long old = -1;
						for(XEvent event: trace){
							XAttributeMap eventAttributeMap = event.getAttributes();
							long current = XTimeExtension.instance().extractTimestamp(event).getTime();
							if(current<=old){
								old = old +1;
							}else{
								old = current;
							}
							String line = "<?xml version=\"1.0\"?><Entry><ProcessID>0</ProcessID><ProcessInstanceID>"+XConceptExtension.instance().extractName(trace)+"</ProcessInstanceID><ModelID>"+broker.readAssignment().getName()+"</ModelID><WorkflowModelElement>"+XConceptExtension.instance().extractName(event).replaceAll(" ", "_")+"</WorkflowModelElement><Timestamp>"+old+"</Timestamp><EventType>"+eventAttributeMap.get(XLifecycleExtension.KEY_TRANSITION)+"</EventType></Entry>";

							Vector<String> eventsForOld = null;
							if(events.containsKey(old)){
								eventsForOld = events.get(old);
							}else{
								eventsForOld = new Vector<String>();
							}
							if(!timestamps.contains(old)){
								timestamps.add(old);
							}
							eventsForOld.add(line);
							events.put(old, eventsForOld);
							//							System.out.println(line);
							//							writeOnTheSocket.println(line);
							//							writeOnTheSocket.flush();
							//							Thread.sleep(5000);
						}
						if(check.isSelected()){
							String line = "<?xml version=\"1.0\"?><Entry><ProcessID>0</ProcessID><ProcessInstanceID>"+XConceptExtension.instance().extractName(trace)+"</ProcessInstanceID><ModelID>"+broker.readAssignment().getName()+"</ModelID><WorkflowModelElement>complete</WorkflowModelElement><Timestamp>"+(old+1)+"</Timestamp><EventType>complete</EventType></Entry>";
							Vector<String> eventsForOld = null;
							if(events.containsKey(old)){
								eventsForOld = events.get(old);
							}else{
								eventsForOld = new Vector<String>();
							}
							if(!timestamps.contains(old)){
								timestamps.add(old);
							}
							eventsForOld.add(line);
							events.put(old, eventsForOld);
						}
					}
					Collections.sort(timestamps);
					for(Long l : timestamps){
						for(String line : events.get(l)){
							System.out.println(line);
							writeOnTheSocket.println(line);
							writeOnTheSocket.flush();
							Thread.sleep(times);
						}
					}
					return;
				}catch (Exception ex){ex.printStackTrace();} 

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
		JPanel optionPanel = factory.createRoundedPanel();
		JPanel radioPanel = factory.createRoundedPanel();
		radioPanel.setBorder(new TitledBorder("Speed"));
		optionPanel.setLayout(new BorderLayout());
		check = factory.createCheckBox("Complete traces in the log", true);
		sixty = factory.createRadioButton("60 ev. per min.");
		twentyfour = factory.createRadioButton("24 ev. per min.");
		twelve = factory.createRadioButton("12 ev. per min.");
		six = factory.createRadioButton("6 ev. per min.");
		three = factory.createRadioButton("3 ev. per min.");
		ButtonGroup bg = new ButtonGroup();
		bg.add(sixty);
		bg.add(twentyfour);
		bg.add(twelve);
		bg.add(six);
		bg.add(three);
		twelve.setSelected(true);
		radioPanel.setLayout(new TableLayout(new double[][] { { 300 }, {20, 20, 20, 20, 20 } }));
		radioPanel.add(sixty, "0,0");
		radioPanel.add(twentyfour, "0,1");
		radioPanel.add(twelve, "0,2");
		radioPanel.add(six, "0,3");
		radioPanel.add(three, "0,4");
		optionPanel.add(check, BorderLayout.EAST);
		optionPanel.add(radioPanel, BorderLayout.WEST);
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.add(optionPanel, BorderLayout.NORTH);
		buttonPanel.add(loadLog, BorderLayout.WEST);
		buttonPanel.add(loadMap, BorderLayout.CENTER);
		buttonPanel.add(startReplay, BorderLayout.EAST);
		contentPane = factory.createRoundedPanel();


		contentPane.setLayout(new BorderLayout());
		contentPane.add(buttonPanel, BorderLayout.SOUTH);
		contentPane.add(instancesContainer, BorderLayout.CENTER);
		contentPane.setSize(200, 800);
		contentPane.setPreferredSize(new Dimension(200,800));
		contentPane.setMaximumSize(new Dimension(200,800));
		contentPane.setMinimumSize(new Dimension(200,800));
		contentPane.setBackground(backgroundColor);
		setContentPane(contentPane);
		setSize(600, 700);
		setPreferredSize(new Dimension(600,700));
		setMaximumSize(new Dimension(600,700));
		setMinimumSize(new Dimension(600,700));
		setName("Log Replayer");
		setResizable(false);
		setTitle("Log Replayer");

	}


	public String getStringFromDoc(org.w3c.dom.Document doc)    {
		DOMImplementationLS domImplementation = (DOMImplementationLS) doc.getImplementation();
		LSSerializer lsSerializer = domImplementation.createLSSerializer();
		return lsSerializer.writeToString(doc);   
	}


	public static void main(String[] args) {
		try {
			if(args.length>0){
				new LogReplayer(args[0]).setVisible(true);
			}else{
				new LogReplayer("conf.xml").setVisible(true);
			}
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




}


