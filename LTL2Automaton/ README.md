Implementation of Probabilistic Business Constraints Monitoring
====================

The implementation is packaged into a standalone file LTL2Automaton.jar. It takes the following parameters:
	
	usage: java -jar .\LTL2Automaton.jar
	-l,--log <arg>     input log file
	-m,--model <arg>   input model path


Input examples are given in files inputLog.xes and inputModel.txt

Implementation is done in Java 1.8

The visualisation of the results is implemented in a larger application called RuM which is located here: https://bitbucket.org/doorless1634/thesis/src/alman-thesis/
