package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.processmining.ltl2automaton.plugins.automaton.Automaton;
import org.processmining.ltl2automaton.plugins.automaton.DefaultAutomatonFactory;
import org.processmining.ltl2automaton.plugins.automaton.State;
import org.processmining.ltl2automaton.plugins.automaton.Transition;
import org.processmining.ltl2automaton.plugins.automaton.TreeAutomaton;
import org.processmining.ltl2automaton.plugins.automaton.TreeAutomatonState;
import org.processmining.ltl2automaton.plugins.automaton.TreeAutomatonTransition;
import org.processmining.ltl2automaton.plugins.formula.DefaultParser;
import org.processmining.ltl2automaton.plugins.formula.Formula;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionFactory;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionTreeLeaf;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionTreeNode;
import org.processmining.ltl2automaton.plugins.formula.conjunction.DefaultTreeFactory;
import org.processmining.ltl2automaton.plugins.formula.conjunction.GroupedTreeConjunction;
import org.processmining.ltl2automaton.plugins.formula.conjunction.TreeFactory;
import org.processmining.ltl2automaton.plugins.ltl.SyntaxParserException;

public class BuildTreeAutomaton {

	public static void main(String[] args) {
		//String formula = "[](A->(X(<>(B))))";
		//String formula = "<>A";

		String formula = "[](!B)\\/(!B U A)";

		List<Formula> formulaeParsed = new ArrayList<Formula>();
		try {
			formulaeParsed.add(new DefaultParser(formula).parse());
		} catch (SyntaxParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TreeFactory<ConjunctionTreeNode, ConjunctionTreeLeaf> treeFactory = DefaultTreeFactory.getInstance();
		ConjunctionFactory<? extends GroupedTreeConjunction> conjunctionFactory = GroupedTreeConjunction
				.getFactory(treeFactory);
		GroupedTreeConjunction conjunction = conjunctionFactory.instance(formulaeParsed);
		Automaton aut = conjunction.getAutomaton();
		//ExecutableAutomaton execAut = new ExecutableAutomaton(aut);
		//execAut.ini();
		//	System.out.println("---------- POSITIVE ----------------");
		//	for(State state : aut){
		//		System.out.print(state);
		//		System.out.println(" "+state.isAccepting());
		//		for(org.processmining.ltl2automaton.plugins.automaton.Transition t : state.getOutput()){
		//			System.out.println(t.getSource()+"  "+t.getPositiveLabel()+ "  "+ t.getTarget() );
		//		}
		//	}
		//	System.out.println("-----------------------------------");

		try {
			formulaeParsed.add(new DefaultParser(formula).parse());
		} catch (SyntaxParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String negatedFormula = "!("+formula+")";
		List<Formula> negformulaeParsed = new ArrayList<Formula>();
		try {
			negformulaeParsed.add(new DefaultParser(negatedFormula).parse());
		} catch (SyntaxParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GroupedTreeConjunction negconjunction = conjunctionFactory.instance(negformulaeParsed);
		Automaton negaut = negconjunction.getAutomaton();
		//ExecutableAutomaton execAut = new ExecutableAutomaton(aut);
		//execAut.ini();

		DefaultAutomatonFactory fact = new DefaultAutomatonFactory();
		//	Automaton unionaut = fact.getAutomaton();




		//		System.out.println("---------- NEG ----------------");
		//		for(State state : negaut){
		//			System.out.println(state.getId());
		//			System.out.println(state.isAccepting());
		//			for(org.processmining.ltl2automaton.plugins.automaton.Transition t : state.getOutput()){
		//				System.out.println(t.getSource()+"  "+t.getPositiveLabel()+ "  "+ t.getTarget() );

		//			}

		//		}
		//		System.out.println(negaut.op.);
		System.out.println("---------- TREE AUTOMATON ----------------");

		TreeAutomaton treeAut = new TreeAutomaton();
		TreeAutomatonState initState = new TreeAutomatonState(0,false);
		treeAut.addState(initState);
		TreeAutomatonState initPos = new TreeAutomatonState(1,aut.getInit().isAccepting());
		treeAut.addState(initPos);
		TreeAutomatonTransition tStartPos = new TreeAutomatonTransition(initState, initPos, "startPos");
		treeAut.addTransitions(tStartPos);

		
		int i = 1;
		HashMap<Integer, Integer> idMapping = new HashMap<Integer, Integer>();
		idMapping.put(aut.getInit().getId(), 1);
		//idMapping.put(negaut.getInit().getId(), 2);
		for(State s : aut){
			if(!s.equals(aut.getInit())){
				i++;
				TreeAutomatonState state = new TreeAutomatonState(i,s.isAccepting());
				treeAut.addState(state);
				idMapping.put(s.getId(),i);
			}
		}
		for(State s : aut){
			for(Transition t : s.getOutput()){
				if(t.isPositive()){
					TreeAutomatonTransition tt = new TreeAutomatonTransition(treeAut.getStates()
							.get(idMapping.get(t.getSource().getId())), treeAut.getStates()
							.get(idMapping.get(t.getTarget().getId())), t.getPositiveLabel());
					treeAut.addTransitions(tt);
				}else if(t.isNegative()){
					if(t.getNegativeLabels()!=null){
						for(String negLabel : t.getNegativeLabels()){
							TreeAutomatonTransition	tt = new TreeAutomatonTransition(treeAut.getStates()
									.get(idMapping.get(t.getSource().getId())), treeAut.getStates()
									.get(idMapping.get(t.getTarget().getId())), "!" + negLabel);
							treeAut.addTransitions(tt);
						}
					}
				}else{
					TreeAutomatonTransition tt = new TreeAutomatonTransition(treeAut.getStates()
							.get(idMapping.get(t.getSource().getId())), treeAut.getStates()
							.get(idMapping.get(t.getTarget().getId())), t.getPositiveLabel());
					treeAut.addTransitions(tt);
				}
			}
		}
		
		idMapping = new HashMap<Integer, Integer>();
		i++;
		idMapping.put(negaut.getInit().getId(), i);
		TreeAutomatonState initNeg = new TreeAutomatonState(i, negaut.getInit().isAccepting());
		treeAut.addState(initNeg);
		TreeAutomatonTransition tStartNeg = new TreeAutomatonTransition(initState, initNeg, "startNeg");
		treeAut.addTransitions(tStartNeg);

		
		for(State s : negaut){
			if(!s.equals(negaut.getInit())){
				i++;
				TreeAutomatonState state = new TreeAutomatonState(i,s.isAccepting());
				treeAut.addState(state);
				idMapping.put(s.getId(),i);
			}
		}

		for(State s : negaut){
			for(Transition t : s.getOutput()){
				if(t.isPositive()){
					TreeAutomatonTransition tt = new TreeAutomatonTransition(treeAut.getStates()
							.get(idMapping.get(t.getSource().getId())), treeAut.getStates()
							.get(idMapping.get(t.getTarget().getId())), t.getPositiveLabel());
					treeAut.addTransitions(tt);
				}else if(t.isNegative()){
					if(t.getNegativeLabels()!=null){
						for(String negLabel : t.getNegativeLabels()){
							TreeAutomatonTransition	tt = new TreeAutomatonTransition(treeAut.getStates()
									.get(idMapping.get(t.getSource().getId())), treeAut.getStates()
									.get(idMapping.get(t.getTarget().getId())), "!" + negLabel);
							treeAut.addTransitions(tt);
						}
					}
				}else{
					TreeAutomatonTransition tt = new TreeAutomatonTransition(treeAut.getStates()
							.get(idMapping.get(t.getSource().getId())), treeAut.getStates()
							.get(idMapping.get(t.getTarget().getId())), t.getPositiveLabel());
					treeAut.addTransitions(tt);
				}			}
		}


		//		System.out.println(treeAut);
		HashMap<Integer,State> idStateMapping = new HashMap<Integer,State>();


		for(TreeAutomatonState s : treeAut.getStates()){
			State state = (State) fact.createState();
			fact.updateState(state, s.getId(), s.isAccepting());
			idStateMapping.put(s.getId(), state);
			if(s.getId()==0){
				fact.initialState(state);
			}
		}
		fact.getAutomaton().getTransitionCount();
		for(TreeAutomatonTransition t : treeAut.getTransitions()){
			Transition transition = new Transition(idStateMapping.get(t.getSource().getId()), 
					idStateMapping.get(t.getTarget().getId()), t.getLabel());
			fact.addTransition(transition);
		}

		System.out.println("#####");


		for(State state : fact.getAutomaton()){
			System.out.print(state);
			if(state.isAccepting())
				System.out.println("(final)");
			if(!state.isAccepting())
				System.out.println("(non-final)");
		}
		for(Transition t : fact.getAutomaton().transitions()){
			System.out.println(t.getPositiveLabel()+": "+t.getSource()+" ->  "+ t.getTarget() );
		}


	}

	static void printTransitions(Automaton aut){
		for(State s : aut){
			System.out.println("output of " +s);
			for(Transition t : s.getOutput()){
				System.out.println(t.getPositiveLabel());
			}
		}
	}
}