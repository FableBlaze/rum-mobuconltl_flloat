package main;

import org.processmining.ltl2automaton.plugins.automaton.Automaton;
import org.processmining.plugins.declareminer.ExecutableAutomaton;

public class VisualizationItem {

	private String label;
	private String variable;
	private double minProbability;
	private double maxProbability;
	private Automaton automaton;
	private ExecutableAutomaton state;
	private String truthValue;
	
	
	
	public String getVariable() {
		return variable;
	}
	public void setVariable(String variable) {
		this.variable = variable;
	}
	public String getTruthValue() {
		return truthValue;
	}
	public void setTruthValue(String truthValue) {
		this.truthValue = truthValue;
	}
	public ExecutableAutomaton getState() {
		return state;
	}
	public void setState(ExecutableAutomaton state) {
		this.state = state;
	}
	public Automaton getAutomaton() {
		return automaton;
	}
	public void setAutomaton(Automaton automaton) {
		this.automaton = automaton;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public double getMinProbability() {
		return minProbability;
	}
	public void setMinProbability(double minProbability) {
		this.minProbability = minProbability;
	}
	public double getMaxProbability() {
		return maxProbability;
	}
	public void setMaxProbability(double maxProbability) {
		this.maxProbability = maxProbability;
	}
	
	
	
}
