package org.processmining.utils;

/**
 * Created by Ubaier on 09/04/2016.
 */
public class NumbersUtil {

    public static Double parseDoubleSafely(String s){
        Double d = null;

        try{
            d = Double.valueOf(s);
        } catch (Exception e){
            //System.out.println("could not parseDoubleSafely" + s);
            //e.printStackTrace();
        }

        return d;
    }

}
