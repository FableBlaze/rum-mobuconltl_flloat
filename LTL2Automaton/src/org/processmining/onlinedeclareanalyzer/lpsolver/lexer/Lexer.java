package org.processmining.onlinedeclareanalyzer.lpsolver.lexer;


import org.processmining.framework.util.Pair;

import java.io.*;

public class Lexer {
	private StreamTokenizer input;

	private Pair<Integer,String> symbol = new Pair<>(NONE,"");
	public static final int EOL = -3;
	public static final int EOF = -2;
	public static final int INVALID = -1;

	public static final int NONE = 0;

    public static final int LEFT = 1;
    public static final int RIGHT = 2;
	public static final int OR = 3;
	public static final int AND = 4;
	public static final int NOT = 5;
	public static final int GT = 6;
	public static final int GTE = 7;
	public static final int LT = 8;
	public static final int LTE = 9;
	public static final int EQUAL = 10;
	public static final int NOT_EQUAL = 11;
	public static final int VARIABLE = 12;
	public static final int PLUS = 13;
	public static final int MINUS = 15;

	public static final String GT_LITERAL = ">";
	public static final String GTE_LITERAL = ">=";
	public static final String LT_LITERAL = "<";
	public static final String LTE_LITERAL = "<=";
	public static final String NE_LITERAL = "!=";
	public static final String EQUAL_LITERAL = "==";
	public static final String NOT_LITERAL = "!";
	public static final String AND_LITERAL = "&";
	public static final String AND_LITERAL2 = "&&";
	public static final String OR_LITERAL = "|";
	public static final String OR_LITERAL2 = "||";
	public static final String PLUS_LITERAL = "+";
	public static final String MINUS_LITERAL = "-";

	public Lexer(InputStream in) {
		Reader r = new BufferedReader(new InputStreamReader(in));
		input = new StreamTokenizer(r);

		input.resetSyntax();
		input.wordChars('a', 'z');
		input.wordChars('A', 'Z');
		input.wordChars(':', ':');
		input.wordChars('_', '_');
		input.wordChars('0', '9');
		input.wordChars('.', '.');
        input.wordChars('>', '>');
        input.wordChars('<', '<');
        input.wordChars('=', '=');
        input.wordChars('-', '-');
        input.wordChars('\'', '\'');
        input.wordChars('!', '!');
        input.wordChars('|', '|');
        input.wordChars('&', '&');

		input.whitespaceChars('\u0000', ' ');
		input.whitespaceChars('\n', '\t');

		input.ordinaryChar('(');
		input.ordinaryChar(')');
        input.ordinaryChar('+');
        //input.ordinaryChar('-');
	}

	public Pair<Integer,String> nextSymbol() {
		try {
			switch (input.nextToken()) {
				case StreamTokenizer.TT_EOL:
					symbol = new Pair<>(EOL,"") ;
					break;
				case StreamTokenizer.TT_EOF:
					symbol = new Pair<>(EOF,"");
					break;
				case StreamTokenizer.TT_WORD: {
                    String s = input.sval;
                    switch (s){
                        case NOT_LITERAL:
                            symbol = new Pair<>(NOT,s);
                            break;
                        case AND_LITERAL:
                        case AND_LITERAL2:
                            symbol = new Pair<>(AND,s);
                            break;
                        case OR_LITERAL:
                        case OR_LITERAL2:
                            symbol = new Pair<>(OR,s);
                            break;
                        case GT_LITERAL:
                            symbol = new Pair<>(GT,s);
                            break;
                        case GTE_LITERAL:
                            symbol = new Pair<>(GTE,s);
                            break;
                        case LT_LITERAL:
                            symbol = new Pair<>(LT,s);
                            break;
                        case LTE_LITERAL:
                            symbol = new Pair<>(LTE,s);
                            break;
                        case NE_LITERAL:
                            symbol = new Pair<>(NOT_EQUAL,s);
                            break;
                        case EQUAL_LITERAL:
                            symbol = new Pair<>(EQUAL,s);
                            break;
                        default:
                            symbol = new Pair<>(VARIABLE,s);

                    }
					break;
				}
                case '+':
                    symbol = new Pair<>(PLUS, PLUS_LITERAL);
                    break;
//                case '-':
//                    symbol = new Pair<>(MINUS, MINUS_LITERAL);
//                    break;
				case '(':
                    symbol = new Pair<>(LEFT,"");
					break;
				case ')':
                    symbol = new Pair<>(RIGHT,"");
					break;
				default:
                    symbol = new Pair<>(INVALID,"");
			}
		} catch (IOException e) {
            symbol = new Pair<>(EOF,"");
		}

		return symbol;
	}

	public String toString() {
		return input.toString();
	}

	public static void main(String[] args) {
		String expression = "true & ((true | false) & !(true & false))";
		Lexer l = new Lexer(new ByteArrayInputStream(expression.getBytes()));
        Pair<Integer,String> s = new Pair<>(-1,"");

	}
}
