package org.processmining.onlinedeclareanalyzer.lpsolver.ast.nonterminal;

import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.Ma;
import org.processmining.framework.util.Pair;

import java.util.ArrayList;

/**
 * Created by Ubaier on 14/04/2016.
 */
public class Lexp extends Ma {
    @Override
    public ArrayList<ArrayList<BooleanExpression>> interpret() {
        ArrayList<BooleanExpression> list = new ArrayList<>();
        list.add(this);
        ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
        out.add(list);
        return out;
    }

    @Override
    public ArrayList<ArrayList<BooleanExpression>> invertInterpret() {
        final Co co = new Co();
        co.setLeft(this.left);
        co.setRight(this.right);
        String symbol = invertSymbol(this.symbol);
        co.setSymbol(symbol);
        ArrayList<BooleanExpression> list = new ArrayList<>();
        list.add(co);
        ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
        out.add(list);
        return out;
    }

    @Override
    public ArrayList<String> traverse() {
        ArrayList<String> out = new ArrayList<>();
        out.add(this.toString());
        return out;
    }

    @Override
    public ArrayList<String> invertTraverse() {
        final Co co = new Co();
        co.setLeft(this.left);
        co.setRight(this.right);
        String symbol = invertSymbol(this.symbol);
        co.setSymbol(symbol);
        ArrayList<String> out = new ArrayList<>();
        out.add(co.toString());
        return out;
    }

    private String invertSymbol(String s) {
        if (s.contains(">")) {
            s = s.replace(">", "<");
        } else if (s.contains("<")) {
            s = s.replace("<", ">");
        } else if (s.equals("!=")) {
            s = "==";
        } else if (s.equals("==")) {
            s = "!=";
        }
        return s;
    }


    public String toString() {
        return String.format("(%s %s %s)", left, symbol, right);
    }

    @Override
    public ArrayList<Pair<String, Integer>> getPairs(int lm) {
        ArrayList<Pair<String, Integer>> output = new ArrayList<>();
        int m = symbol.equals("-") ? -1 : 1;
        if (getLeft() instanceof Lexp) {
            Lexp l = (Lexp) getLeft();
            output.addAll(l.getPairs(lm));
        } else {
            Pair<String, Integer> pl = new Pair<>(getLeft().toString(), lm);
            output.add(pl);
        }

        if (getRight() instanceof Lexp) {
            Lexp l = (Lexp) getRight();
            output.addAll(l.getPairs(m));
        } else {
            Pair<String, Integer> pl = new Pair<>(getRight().toString(),lm * m);
            output.add(pl);
        }

        return output;
    }
}
