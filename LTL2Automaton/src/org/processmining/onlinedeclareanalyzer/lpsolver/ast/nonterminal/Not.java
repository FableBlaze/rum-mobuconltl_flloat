package org.processmining.onlinedeclareanalyzer.lpsolver.ast.nonterminal;


import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.NonTerminal;

import java.util.ArrayList;

public class Not extends NonTerminal {
	public void setChild(BooleanExpression child) {
		setLeft(child);
	}

	public void setRight(BooleanExpression right) {
		throw new UnsupportedOperationException();
	}

	public ArrayList<ArrayList<BooleanExpression>> interpret() {
		ArrayList<ArrayList<BooleanExpression>> r = left.invertInterpret();
		ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
		out.addAll(r);
		return out;
	}

	@Override
	public ArrayList<ArrayList<BooleanExpression>> invertInterpret() {
		ArrayList<ArrayList<BooleanExpression>> r = left.interpret();
		ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
		out.addAll(r);
		return out;
	}

	@Override
	public ArrayList<String> traverse() {
		ArrayList<String> out = new ArrayList<>();
		ArrayList<String> r = left.invertTraverse();
		out.addAll(r);
		return out;
	}

	@Override
	public ArrayList<String> invertTraverse() {
		ArrayList<String> out = new ArrayList<>();
		ArrayList<String> r = left.traverse();
		out.addAll(r);
		return out;
	}


	public String toString() {
		return String.format("!%s", left);
	}
}
