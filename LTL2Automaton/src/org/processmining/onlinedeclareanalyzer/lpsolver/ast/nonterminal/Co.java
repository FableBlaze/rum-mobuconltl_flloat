package org.processmining.onlinedeclareanalyzer.lpsolver.ast.nonterminal;


import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.Comparison;

import java.util.ArrayList;

/**
 * Created by Ubaier on 02/04/2016.
 */
public class Co extends Comparison {

    @Override
    public ArrayList<ArrayList<BooleanExpression>> interpret() {
        ArrayList<BooleanExpression> list = new ArrayList<>();
        ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
        if(this.getSymbol().equals("!=")){
            Co lCo = new Co();
            lCo.setLeft(this.left);
            lCo.setRight(this.right);
            lCo.setSymbol("<");

            Co rCo = new Co();
            rCo.setLeft(this.left);
            rCo.setRight(this.right);
            rCo.setSymbol(">");
            Or or = new Or();
            or.setLeft(lCo);
            or.setRight(rCo);
            return or.interpret();
        } else {
            list.add(this);
            out.add(list);
        }
        return out;
    }

    @Override
    public ArrayList<ArrayList<BooleanExpression>> invertInterpret() {
        final Co co = new Co();
        co.setLeft(this.left);
        co.setRight(this.right);
        String symbol = invertSymbol(this.symbol);
        co.setSymbol(symbol);

        if(co.getSymbol().equals("!=")){
            Co lCo = new Co();
            lCo.setLeft(co.left);
            lCo.setRight(co.right);
            lCo.setSymbol("<");

            Co rCo = new Co();
            rCo.setLeft(co.left);
            rCo.setRight(co.right);
            rCo.setSymbol(">");
            Or or = new Or();
            or.setLeft(lCo);
            or.setRight(rCo);
            return or.interpret();
        }

        ArrayList<BooleanExpression> list = new ArrayList<>();
        list.add(co);
        ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
        out.add(list);
        return out;
    }

    @Override
    public ArrayList<String> traverse() {

        if(this.getSymbol().equals("!=")){
            Co lCo = new Co();
            lCo.setLeft(this.left);
            lCo.setRight(this.right);
            lCo.setSymbol("<");

            Co rCo = new Co();
            rCo.setLeft(this.left);
            rCo.setRight(this.right);
            rCo.setSymbol(">");
            Or or = new Or();
            or.setLeft(lCo);
            or.setRight(rCo);
            return or.traverse();
        }

        ArrayList<String> out = new ArrayList<>();
        out.add(this.toString());
        return out;
    }

    @Override
    public ArrayList<String> invertTraverse() {
        final Co co = new Co();
        co.setLeft(this.left);
        co.setRight(this.right);
        String symbol = invertSymbol(this.symbol);
        co.setSymbol(symbol);

        if(co.getSymbol().equals("!=")){
            Co lCo = new Co();
            lCo.setLeft(co.left);
            lCo.setRight(co.right);
            lCo.setSymbol("<");

            Co rCo = new Co();
            rCo.setLeft(co.left);
            rCo.setRight(co.right);
            rCo.setSymbol(">");
            Or or = new Or();
            or.setLeft(lCo);
            or.setRight(rCo);
            return or.traverse();
        }
        ArrayList<String> out = new ArrayList<>();
        out.add(co.toString());
        return out;
    }

    private String invertSymbol(String s) {
        if (s.contains(">")) {
            s = s.replace(">", "<");
        } else if (s.contains("<")) {
            s = s.replace("<", ">");
        } else if (s.equals("!=")) {
            s = "==";
        } else if (s.equals("==")) {
            s = "!=";
        }
        return s;
    }


    public String toString() {
        return String.format("(%s %s %s)", left, symbol, right);
    }
}
