package org.processmining.onlinedeclareanalyzer.lpsolver.ast;



import org.processmining.framework.util.Pair;

import java.util.ArrayList;

/**
 * Created by Ubaier on 14/04/2016.
 */
public abstract class  Ma  implements BooleanExpression {
    protected BooleanExpression left, right;
    protected String symbol;

    public void setLeft(BooleanExpression left) {
        this.left = left;
    }

    public void setRight(BooleanExpression right) {
        this.right = right;
    }

    public void setSymbol(String symbol){
        this.symbol = symbol;
    }

    public BooleanExpression getLeft() {
        return left;
    }

    public BooleanExpression getRight() {
        return right;
    }

    public abstract ArrayList<Pair<String, Integer>> getPairs(int lm);

    public String getSymbol() {
        return symbol;
    }
}
