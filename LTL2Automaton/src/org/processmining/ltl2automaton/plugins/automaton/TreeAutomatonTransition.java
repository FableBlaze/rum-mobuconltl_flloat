package org.processmining.ltl2automaton.plugins.automaton;

public class TreeAutomatonTransition {
	TreeAutomatonState source;
	TreeAutomatonState target;
	String label;
	
	public TreeAutomatonTransition(TreeAutomatonState source, TreeAutomatonState target, String label) {
		this.source = source;
		this.target = target;
		this.label = label;
	}

	public TreeAutomatonState getSource() {
		return source;
	}

	public void setSource(TreeAutomatonState source) {
		this.source = source;
	}

	public TreeAutomatonState getTarget() {
		return target;
	}

	public void setTarget(TreeAutomatonState target) {
		this.target = target;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
