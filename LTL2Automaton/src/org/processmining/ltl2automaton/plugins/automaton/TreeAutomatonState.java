package org.processmining.ltl2automaton.plugins.automaton;

public class TreeAutomatonState {
	boolean isAccepting;
	int id;
	
	public TreeAutomatonState (int id, boolean isAccepting){
		this.id = id;
		this.isAccepting = isAccepting;
	}
	
	public boolean isAccepting() {
		return isAccepting;
	}
	public void setAccepting(boolean isAccepting) {
		this.isAccepting = isAccepting;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
